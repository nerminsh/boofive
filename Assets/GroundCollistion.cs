﻿using UnityEngine;
using System.Collections;

public class GroundCollistion : MonoBehaviour {
	Rigidbody2D rBody;
	void Start(){
		rBody = GetComponent<Rigidbody2D> ();
	}
	// Use this for initialization
	void OnCollisionStay2D(Collision2D ground)
    {
        if (ground.gameObject.tag=="Ocean")
        {
			rBody.isKinematic = true;
        }
    }
}
