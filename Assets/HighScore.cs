﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class HighScore : MonoBehaviour {
    Text highScore;
	// Use this for initialization
	void Start () {
        highScore = GetComponent<Text>();
        highScore.text="HIGHSCORE "+ Game.LoadHighScore();
	}
}
