﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class AllCoins : MonoBehaviour {

    Text Coins;
    // Use this for initialization
    void Start()
    {
        Coins = GetComponent<Text>();
        Coins.text = "COINS "+Game.LoadCoins();
    }
}
