﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.transform.parent)
        {
            Destroy(other.transform.parent.gameObject);
            Game.Obstacles.Remove(other.transform.parent.gameObject);
            

        }
        else
        {
            Destroy(other.gameObject);
            Game.Obstacles.Remove(other.gameObject);
        }
    }
}
