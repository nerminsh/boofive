﻿using UnityEngine;
using System.Collections;

public class BackgroundCamera : MonoBehaviour {
	public Color color1;
	public Color color2;
    Camera cam;
	void Awake () {
	}
	void Start () {
        cam = GetComponent<Camera>();
    }
	void Update () {
		if (transform.position.y > -4) {
            cam.backgroundColor = color1;
		} 

		else {
            cam.backgroundColor = color2;
		}
	}
}
