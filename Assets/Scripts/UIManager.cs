﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour {

    public GameObject Play;
    public GameObject Swipe;
    public GameObject Tutorial;
    public GameObject Emmiter;
    public static bool gameR = false;
    public static bool gameL = false;
    public bool music = true;
    public bool sound = true;
    public bool fill = false;
	public NEmmiter nEmmiter;
    public Sprite noMusic;
    public Sprite noSound;
    public Sprite yesMusic;
    public Sprite yesSound;
    public GameObject PowerUpText;
    public GameObject PowerUpImage;
	Image Mmusic;
	Image Msound;
	Image Pmusic;
	Image Psound;

    float thold = 0.3f;
    float tRec = -1f;

    public GameObject socialBar;
    public GameObject socialBarButton;
    public GameObject settingBar;
    public GameObject settingBarButton;
    Image socialImg;
    Image settingImg;
	SettingBar stingbr;
	SocialBar scialbr;


    SocialBar socialBarClass;
    SettingBar settingBarClass;
	public GameObject Main;
	public GameObject Paused;
	public GameObject Died;
	public GameObject HUD;
	public GameObject SubHealth;
    bool social = true;
    GameObject soundMan;
    Game game;


    void Start()
    {

        game = GameObject.Find("Game").GetComponent<Game>();
        soundMan = GameObject.FindGameObjectWithTag("SoundMan");
        //socialBar.gameObject.SetActive(false);
        socialImg= socialBar.GetComponent<Image>();
        settingImg = settingBar.GetComponent<Image>();
		stingbr = settingBar.GetComponent<SettingBar> ();
		scialbr = socialBar.GetComponent<SocialBar> ();
		nEmmiter = Emmiter.GetComponent<NEmmiter> ();
         if (PlayerPrefs.GetInt("Music") != 0)
         {
             if (PlayerPrefs.GetInt("Music") == 1)
             {
                 music = soundMan.GetComponent<Sounds>().music = true;
                 Main.transform.GetChild(0).GetComponent<Image>().sprite = yesMusic;
             }
             else
             {
                 music = soundMan.GetComponent<Sounds>().music = false;
                 Main.transform.GetChild(0).GetComponent<Image>().sprite = noMusic;
             }
         }
		Mmusic = Main.transform.GetChild (0).GetComponent<Image> ();
		Pmusic = Paused.transform.GetChild (0).GetComponent<Image> ();

		Msound = Main.transform.GetChild (1).GetComponent<Image> ();
		Psound = Paused.transform.GetChild (1).GetComponent<Image> ();

         if (PlayerPrefs.GetInt("Sound") != 0)
         {
             if (PlayerPrefs.GetInt("Sound") == 2)
             {
                 sound = soundMan.GetComponent<Sounds>().sound = false;
                 Msound.sprite = noSound;
             }
             else
             {
                 sound = soundMan.GetComponent<Sounds>().sound = true;
                 Msound.sprite = yesSound;
             }
         }

    }
    
        void Update()
    {

        if (Game.bom.hasPowerUp && Game.bom.isProtected==false)//&& Game.bom.CurrentPowerUp.GetTime()>0)
        {
            PowerUpText.SetActive(true);
            PowerUpImage.SetActive(true);
        }
        else
        {
            PowerUpText.SetActive(false);
            PowerUpImage.SetActive(false);

        }

        if (Game.BombaDied)
        {
            soundMan.transform.position = new Vector3(0, 0, 60f);
            Died.SetActive(true);
            Bomba.canShoot = false;
            HUD.SetActive(false);
            Time.timeScale = 0;
        }
        if (gameL && gameR)
        {
            PlayFromTutorial();
            game.enabled = true;
            Emmiter.SetActive(true);
            nEmmiter.Reset();

            //transform.parent.gameObject.AddComponent<MoveBigObs>();
            //transform.parent.gameObject.GetComponent<MoveBigObs>().speed = -100f;

            //foreach (GameObject i in GameObject.Find("Background").transform)
            //{
            //    i.SetActive(true);
            //}

            gameL = false;
            gameR = false;
        }

    }
	public void PlayFromMain(){
		Main.SetActive(false);
		//HUD.SetActive (true);
        Tutorial.SetActive(true);
	}

    public void PlayFromTutorial()
    {
        Tutorial.SetActive(false);
        HUD.SetActive(true);
    }

    public void PanelRightPressed()
    {
        Tutorial.transform.GetChild(0).gameObject.SetActive(false);
        gameR = true;
    }

    public void PanelLeftDown()
    {
        Tutorial.transform.GetChild(1).transform.localScale = new Vector3(0.8f, 0.8f, 1f);
        //print("[UIManager] Clicking!");
        if (tRec == -1)
        {
            tRec = Time.time;
        }
    }

    public void PanelLeftUp()
    {
        if ((Time.time - tRec) > thold)
        {
            //print("[UIManager] Hold finished!");
            Tutorial.transform.GetChild(1).gameObject.SetActive(false);
            gameL = true;
        }
        else
        {
            Tutorial.transform.GetChild(1).transform.localScale = new Vector3(1f, 1f, 1f);
            tRec = -1;
        }
    }

    public void Music()
    {
        //Music will stop
        if (music)
        {
            music = soundMan.GetComponent<Sounds>().music = false;
            PlayerPrefs.SetInt("Music", 2);
            Mmusic.sprite = noMusic;
            Pmusic.sprite = noMusic;
        }
        else
        {
            music = soundMan.GetComponent<Sounds>().music = true;
            PlayerPrefs.SetInt("Music", 1);
            Mmusic.sprite = yesMusic;
            Pmusic.sprite = yesMusic;
        }
    }
	public void Sound()
    {
        //Sound will stop
        if (sound)
        {
            sound = soundMan.GetComponent<Sounds>().sound = false;
            PlayerPrefs.SetInt("Sound", 2);
            Msound.sprite = noSound;
            Psound.sprite = noSound;
        }
        else
        {
            sound = soundMan.GetComponent<Sounds>().sound = true;
            PlayerPrefs.SetInt("Sound", 1);
            Msound.sprite = yesSound;
            Psound.sprite = yesSound;
        }
	}
    public void SocialMedia()
    {

    }
    public void facebookShare()
    {
        SocialMediaMethods.ShareToFacebook();
    }
    public void twitterShare()
    {
        SocialMediaMethods.ShareToTwitter("I've helped Bomba survive for... ");
    }
    public void Pause()
    {
        soundMan.transform.position = new Vector3(0, 0, 60f);
        Bomba.canShoot = false;
        HUD.SetActive(false);
        Paused.SetActive(true);
        if (sound)
        {
            Psound.sprite = yesSound;
        }
        else
        {
            Psound.sprite = noSound;
        }

        if (music)
        {
            Pmusic.sprite = yesMusic;
        }
        else
        {
            Pmusic.sprite = noMusic;
        }

     //   pauseButton.SetActive(false);
      //  playButton.SetActive(true);
        Time.timeScale = 0;
    }
    public void playFromPaused()
    {
        Time.timeScale = 1;
        Bomba.canShoot = true;
        //StartCoroutine(recoverShoot());
        HUD.SetActive(true);
        Paused.SetActive(false);
        soundMan.transform.position = Vector3.zero;
    }
    public void Retry()
    {
        Game.BombaDied = false;
        //Debug.Log("retry");
        Died.SetActive(false);
        Time.timeScale = 1;
        Bomba.canShoot = true;
        Paused.SetActive(false);
        HUD.SetActive(true);
        soundMan.transform.position = Vector3.zero;

        Game.COINS = 0;
        Game.METER = 0;
        Game.bomba.transform.position = new Vector2(-7.78414f, 0);
        nEmmiter.Reset();
    }

    public void Home()
    {
        soundMan.transform.position = Vector3.zero;

        Time.timeScale = 1;
        Bomba.canShoot = true;
        Game.BombaDied = false;
        Application.LoadLevel(0);

    }

    IEnumerator recoverShoot()
    {
        Time.timeScale = 1;
        yield return new WaitForFixedUpdate();
        Bomba.canShoot = true;
    }

    public void PlayPress()
    {

        Play.SetActive(false);
        Swipe.SetActive(true);

    }
    public void SocialBarButton()
    {
        if (social) {
            scialbr.rate = 1f;
            foreach (Transform child in socialImg.gameObject.transform)
            {
                child.gameObject.SetActive(true);
            }
        }
        else
        {
            SocialBar();
        }
        social = !social;
    }
    public void SocialBar()
    {
       // fill = false;
        // fill = false;
        
		scialbr.rate = 0f;
        socialBarButton.gameObject.SetActive(true);
        foreach (Transform child in socialImg.gameObject.transform)
        {
            child.gameObject.SetActive(false);
        }
     
    }
    public void SettingBarButton()
    {
        //  fill = true;
		stingbr.rate = 1f;
        settingBarButton.gameObject.SetActive(false);
        foreach (Transform child in settingImg.gameObject.transform)
        {
            child.gameObject.SetActive(true);
        }
    }
    public void SettingBar()
    {
        
		stingbr.rate = 0f;
        settingBarButton.gameObject.SetActive(true);
        foreach (Transform child in settingImg.gameObject.transform)
        {
            child.gameObject.SetActive(false);
        }
    }
}
