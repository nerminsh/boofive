﻿using UnityEngine;
using System.Collections;

public class BombaLimit : MonoBehaviour {
	public static bool BOMBA_LIMIT = false;
	void OnTriggerEnter2D(Collider2D other)
	{
		
		if (other.tag == "Bomba")
		{
			BOMBA_LIMIT = !BOMBA_LIMIT;
			
		}
	}
}
