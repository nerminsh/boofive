﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SettingBar : MonoBehaviour
{

    public GameObject socialBarButton;
    Button btn;
    Image img;
    public float rate;
    // Use this for initialization
    void Start()
    {
        rate = 0;
        btn = socialBarButton.GetComponent<Button>();
        img = this.GetComponent<Image>();

    }

    // Update is called once per frame
    void Update()
    {
        img.fillAmount = Mathf.MoveTowards(img.fillAmount, rate, .1f);
       

    }
}