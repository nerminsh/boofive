﻿using UnityEngine;
using System.Collections;using UnityEngine.UI;

public class SettingBarButton : MonoBehaviour
{
    public GameObject socialBar;
    Image img;
    SettingBar sb;
    // Use this for initialization
    void Start()
    {
        img = this.GetComponent<Image>();
        sb = socialBar.GetComponent<SettingBar>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            sb.rate = 1f;
            this.gameObject.SetActive(false);

        }


    }
}