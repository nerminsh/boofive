﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SocialBarButton : MonoBehaviour {
    public GameObject socialBar;
    Image img;
    SocialBar sb;
	// Use this for initialization
	void Start () {
        img = this.GetComponent<Image>();
        sb = socialBar.GetComponent<SocialBar>();

    }
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            sb.rate = 1f;
            this.gameObject.SetActive(false);
            foreach (Transform child in socialBar.gameObject.transform)
                {
                    child.gameObject.SetActive(true);
                }

        }
       
	}
    
}
