﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class OverHeatBar : MonoBehaviour {
    int bombaHitCounter;
    Image overHeatBar;
   // Use this for initialization
    void Start()
    {
         overHeatBar = GetComponent<Image>();
         
    }

    // Update is called once per frame
    void Update()
    {
        if (Game.BombaDied==true)
        {
            overHeatBar.fillAmount = 0f;
            Bomba.heatingRatio = 0;
        }
        else
        {
            overHeatBar.fillAmount = Mathf.MoveTowards(overHeatBar.fillAmount, Bomba.heatingRatio, .1f * Bomba.standardHeatingRatio);

        }
    }
   
}
