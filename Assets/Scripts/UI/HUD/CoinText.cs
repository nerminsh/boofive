﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinText : MonoBehaviour
{
     Text coins;
    void Start()
    {
        coins = GetComponent<Text>();
    }
    void Update()
    {

        coins.text = "" + Game.COINS;

    }
    
}
