﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class MeterText : MonoBehaviour {
    Text meter;


    void Start()
    {
        meter = GetComponent<Text>();
    }
    
    void Update()
    {
        meter.text =  Game.METER+"M";
    }
}
