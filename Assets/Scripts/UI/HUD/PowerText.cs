﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PowerText : MonoBehaviour {

    Text PowerUpText;
    int t;
    void Awake()
    {
        PowerUpText = GetComponent<Text>();
    }
    IEnumerator Start()
    {
        t = Game.bom.PowerUpStateMachine.getCurrentPowerUpState().GetTime();
            
            while (t>0)
            {

                        PowerUpText.text = "" + t--;
                        yield return new WaitForSeconds(1);       
            }
    
    }
    
   
}
