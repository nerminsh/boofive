﻿using UnityEngine;
using System.Collections;

public class Emmiter : MonoBehaviour {
    
    //
    
    public int timeMax = 10;
    public int timeMin = 3;
    int slicesCounter = 1;
    int slicesCount = 3;
    int PatternsCount = 0;
    int SubmarineType = 0;
    public GameObject[] GameObjects;
    public GameObject[] Coins;
    public GameObject[] Submarines;
    public GameObject submarine;
    bool sub = false;
    public float time;
    public float CoinsTime;
    public float SubmarinesTime;
    public int i = 0;
    
	// Use this for initialization
	void Start () {
        time = Time.time;
        CoinsTime = int.MaxValue;
        SubmarinesTime = int.MaxValue;
        
	}
	
	// Update is called once per frame
    //void Update () {
	
    //}
    void FixedUpdate()
    {
        if (Time.time >= time )
        {
            Debug.Log("Slices Count = "+slicesCount);
            Debug.Log("Slices Counter = " + slicesCounter);


            GameObject Slice = (GameObject)Instantiate(GameObjects[Random.Range(0,GameObjects.Length)],
                new Vector2(gameObject.transform.position.x, 0),
                Quaternion.identity);
            while (!Slice)
            {
                Slice = (GameObject)Instantiate(GameObjects[Random.Range(0, GameObjects.Length)],
                new Vector2(gameObject.transform.position.x, 0),
                Quaternion.identity);
            }
            if (Slice)
            {
                slicesCounter++;
                Debug.Log(Slice.name);
            }

            if (slicesCounter == slicesCount+1)
            {
                slicesCounter = 0;
                PatternsCount++;
                if (PatternsCount == 3)
                {
                    PatternsCount = 0;
                    CoinsTime = int.MaxValue;
                    SubmarinesTime = Time.time + timeMax / 3;
                    time = int.MaxValue;

                }
                else
                {
                    time = Time.time + timeMax;
                    CoinsTime = Time.time + (timeMax / 2);
                    if (Game.METER < 2000)
                    {
                        slicesCount = Easy();
                    }
                    else if (Game.METER < 4000)
                    {
                        slicesCount = Medium();
                    }
                    else
                    {
                        slicesCount = Hard();
                    }
                }
               
            }
            else
            {
                time = Time.time + timeMin;
            }

        }
        if (Time.time >= SubmarinesTime && Time.time < time )
        {
            sub = true;
            SubmarinesTime = int.MaxValue;
            submarine = (GameObject)Instantiate(Submarines[i++],
               new Vector2(gameObject.transform.position.x, 0),
               Quaternion.identity);
            if (i == Submarines.Length)
            {
                i = 0;
            }
        }
        if (Time.time >= CoinsTime && Time.time < time)
        {
            bool emmitCoins = Random.Range(0, 10) <= 7;
            if (emmitCoins)
            {
                Instantiate(Coins[Random.Range(0,Coins.Length)],
                    new Vector2(gameObject.transform.position.x,
                        Random.Range(-2, 2)),
                        Quaternion.identity);
            }
            CoinsTime = int.MaxValue;
        }
        if (submarine && submarine.GetComponent<Submarine>().finishedAmmo&&sub)
        {
            time = Time.time + timeMax;
            sub = false;
        }
        //Debug.Log(slicesCounter++);

        //GameObject obj = (GameObject)Instantiate(GameObjects[Random.Range(0, 
        //    GameObjects.Length)],new Vector2(gameObject.transform.position.x,0), Quaternion.identity);
        //if (slicesCounter%3==0){
        //    bool emmitCoins = Random.Range(0, 10) <= 7;
        //    if (emmitCoins)
        //    {
        //        Instantiate(Coins[Random.Range(0,GameObjects.Length)],
        //            new Vector2(gameObject.transform.position.x,
        //                Random.Range(-2, 2)),
        //                Quaternion.identity);
        //    }
        //    Invoke("Emmit", timeMax);
        //}
        //else
        //    Invoke("Emmit", timeMin);

    }
    int Easy()
    {
        int randNum = Random.Range(1, 10);
        slicesCounter = 0;
        if (randNum <= 7)
        {
            return 3;
        }
        else if (randNum <= 9)
        {
            return 4;
        }
        else
        {
            return 5;
        }
    }
    int Medium()
    {
        int randNum = Random.Range(1, 10);
        slicesCounter = 0;
        if (randNum <= 7)
        {
            return 4;
        }
        else if (randNum <= 9)
        {
            return 3;
        }
        else
        {
            return 5;
        }
    }
    int Hard()
    {
        int randNum = Random.Range(1, 10);
        slicesCounter = 0;
        if (randNum <= 7)
        {
            return 5;
        }
        else if (randNum <= 9)
        {
            return 4;
        }
        else
        {
            return 3;
        }

    }
}
