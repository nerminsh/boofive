﻿using UnityEngine;
using System.Collections;

public class MoveX : MonoBehaviour {

    // Use this for initialization
    Rigidbody2D rBody;
	void Start () {
        rBody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {

        rBody.velocity = new Vector2 (0.5f * Mathf.Sin(3*Time.time), rBody.velocity.y);
	}
}
