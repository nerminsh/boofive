﻿using UnityEngine;
using System.Collections;

public enum MissionType
{
    CollectCoins,
    Distance,
    ShootObstacles,
    CollectGems
}


public class JsonMission
{
    public string Discription;
    public int Type;
    public int Goal;
    public int Current;
    public int Done;
    public int InOneRun;
    public int Active;
    public int Order;
}

public class Mission : MonoBehaviour {
    public string Discription;
    public MissionType Type;
    public int Goal;
    public int Current;
    public bool Done;
    public bool InOneRun;
    public bool Active;
    public int Order;

    public void SetMission(JsonMission m)
    {
        Discription = m.Discription;
        Type = (MissionType)m.Type;
        Goal = m.Goal;
        Current = m.Current;
        Done = m.Done != 0;
        InOneRun = m.InOneRun != 0;
        Active = m.Active != 0;
        Order = m.Order;
    }
	void Update () {
        
	}
}
