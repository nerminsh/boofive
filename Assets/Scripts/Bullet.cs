﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
    public float speed;
    Rigidbody2D rBody;
	// Use this for initialization
	void Start () {
        //print(speed);
        rBody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        rBody.velocity = (new Vector3(-speed, 0));
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bomba")
        {
            Game.BombaDied = true;
            //Destroy(other.gameObject);
            //Application.LoadLevel(0);
        }
    }
}
