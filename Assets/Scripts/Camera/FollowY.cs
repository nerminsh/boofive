﻿using UnityEngine;
using System.Collections;

public class FollowY : MonoBehaviour
{
    GameObject bomba;
    Transform originalTransform;
    public float smoothRate = 0.5f;
	public Camera BackgroundCamera;
    Rigidbody2D velocity;
    // Use this for initialization
    void Start()
    {
        bomba = Game.bomba;
        velocity = bomba.GetComponent<Rigidbody2D>();
        originalTransform = transform;
        AspectUtility.backgroundCam = BackgroundCamera;

    }
    void Update()
    {
        if (bomba && bomba.transform.position.y > -8 && bomba.transform.position.y < 12)
        {
            originalTransform.position = Vector3.MoveTowards(originalTransform.position,
                new Vector3(0, Game.bombaTransform.position.y,-10), (velocity.velocity.magnitude * 2)/ Time.deltaTime);
        }
    }
}
