﻿using UnityEngine;
using System.Collections;

public class CameraFollowBomba : MonoBehaviour {
    GameObject bomba;
    Transform originalTransform;
   public float smoothRate=0.5f;
   Vector2 velocity;
	// Use this for initialization
	void Start () {
        bomba = Game.bomba;
        originalTransform = transform;
       velocity   = new Vector2(0.5f,0.5f);
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 newPos = Vector2.zero;
      //  newPos.x = Mathf.SmoothDamp(originalTransform.position.x, bomba.transform.position.x+3, ref velocity.x, smoothRate);
        newPos.y = Mathf.SmoothDamp(originalTransform.position.y, bomba.transform.position.y, ref velocity.y, smoothRate);

        Vector3 newPos3d = new Vector3(newPos.x,newPos.y,transform.position.z);
        transform.position = Vector3.MoveTowards(transform.position,newPos3d,Time.time);
	}
}
