﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Ewais : MonoBehaviour {

    // Added by Arshd :D
    // Distance to Upgrade Sub, opt. is 500
    public float subLvlUp = 500;
    // Max Ammo for Sub, for test it's 6
    public int MAX_AMMO = 14;
    // EOA = End of Arsh :D
    public GameObject HealthBar;

    Seeker seek;
    //Arshd instead of Arshad :D
    public bool Arshd;
    int target = 1;
    public GameObject bullit;
    public GameObject Bomba;
    public float ReloadTime;
    public float RateOfFire;
    public int BulletsCount;
    public float NextBulletTime = 0.0f;
    public int Ammo = 3;
    int NoOfBullits = 0;
    public bool start;
    public float Health = 7;
    float h = 0;
    public int num = 4;
    Image hb;
    public GameObject ex;
    Renderer r;
	

	void Start () {
        //HealthBar = GameObject.FindGameObjectWithTag("healthbar");
        hb = HealthBar.GetComponent<Image>();
        hb.fillAmount = 1;
        // Added by Arshd :D
        SetAmmo();
        r = GetComponent<Renderer>();
        // EOA = End of Arsh :D

        NextBulletTime = int.MaxValue;
        
        seek = gameObject.GetComponent<Seeker>();
        if (seek)
        {
            seek.target = new Vector2(5, Bomba.transform.position.y);
            
           
        }
        else
        {
            
           
        }
	}
	
	// Update is called once per frame
	void Update () {

        //HealthBar.transform.position = transform.position;
        if (transform.position.x <= num && start)
        {
            NextBulletTime = Time.time + 2;
            start = false;
        }
        else if (start)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(num, 0), 0.3f);
        }

        if (Time.time >= NextBulletTime)
        {

            Game.Obstacles.Add((GameObject)Instantiate(bullit, new Vector2(transform.position.x -
                r.bounds.size.x/2, transform.position.y),
                Quaternion.identity));
            
            // Added by Arsh :D
            // EOA = End of Arsh :D

            NoOfBullits++;
            if (NoOfBullits == BulletsCount)
            {
                NoOfBullits = 0;
                Ammo--;
                NextBulletTime += ReloadTime;
            }
            else
            {
                NextBulletTime += RateOfFire;
            }
            
        }
        if (h <= 0)
        {
            //Destroy(gameObject.GetComponent<Seeker>());
            //GetComponent<Submarine>().finishedAmmo = true;
            //NextBulletTime = int.MaxValue;
            //GetComponent<Rigidbody2D>().velocity = new Vector2(-5,0);
            //if(Bomba)
            //transform.position = Vector2.MoveTowards(transform.position, 
            //    new Vector2(transform.position.x, Bomba.transform.position.y), 0.05f);
            StartCoroutine("Confused");
        }
        else if (Bomba)
        {
            
            seek.target = new Vector2(num, Bomba.transform.position.y);
        }
        
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.tag == "Bomba")
        {
            //Destroy(other.gameObject);
            Game.BombaDied = true;
            //Application.LoadLevel(0);
        }
        else if (other.tag == "TinyBomb")
        {
            h--;
            hb.fillAmount = h / Health;
            
        }

    }

    IEnumerator Confused()
    {
        NextBulletTime = int.MaxValue;
        ex.SetActive(true);
        yield return new WaitForSeconds(5);
        ex.SetActive(false);

        transform.rotation = Quaternion.Euler(0, 180, 0);
        Destroy(gameObject.GetComponent<Seeker>());
        GetComponent<Submarine>().finishedAmmo = true;
        
        //GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 0);
        //if (Bomba)
        //    transform.position = Vector2.MoveTowards(transform.position,
        //        new Vector2(transform.position.x, Bomba.transform.position.y), 0.05f);
        GetComponent<Rigidbody2D>().AddForce(new Vector2(0.2f, 0), ForceMode2D.Impulse);
        Destroy(gameObject, 2);
    }

    // Added by Arsh :D
    private void SetAmmo()
    {
        Health = (Health + (int)(Game.METER / subLvlUp)) > MAX_AMMO ? MAX_AMMO : Health + (int)(Game.METER / subLvlUp);
        h = Health;
    }

    private void SetBulletSpeed()
    {
        bullit.GetComponent<Bullet>().speed = ((int)(Game.METER / subLvlUp)) + 8f;
    }
    // EOA = End of Arsh :D
    
    //void OnGUI()
    //{
    //    GUI.HorizontalSlider(new Rect(Camera.main.orthographicSize/2, 25, 100, 30), Health, 0.0F, 7);
    //}

}
