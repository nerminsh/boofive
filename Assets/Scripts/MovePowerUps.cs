﻿using UnityEngine;
using System.Collections;

public class MovePowerUps : MonoBehaviour {
	float Zero;
	public float amplitude = 3;
	public float freq = 2;
    Transform myTransform;
    //public float speed = 3;
    // Use this for initialization
    void Start () {
		Zero = transform.position.y;
        myTransform = transform;
    }
	void FixedUpdate(){
		//rigidbody2D.velocity = new Vector2(-speed, rigidbody2D.velocity.y);
		}
	// Update is called once per frame
	void Update () {
        myTransform.position = new Vector2 (myTransform.position.x,Zero+ amplitude * Mathf.Sin(freq * Time.time));
	}
}
