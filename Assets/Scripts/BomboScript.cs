﻿using UnityEngine;
using System.Collections;

public class BomboScript : MonoBehaviour {

    public GameObject tinyBomb;
    private GameObject onlyBomb;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            onlyBomb = Instantiate(tinyBomb, transform.position, Quaternion.identity) as GameObject;
            onlyBomb.transform.parent = transform;
            //onlyBomb.rigidbody2D.AddForce(new Vector2(50.0f, 0f));
            onlyBomb.GetComponent<Rigidbody2D>().velocity = new Vector2(1f, 0f);
        }
	}
}
