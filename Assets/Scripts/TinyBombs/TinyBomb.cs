﻿using UnityEngine;
using System.Collections;

public class TinyBomb : TinyBombState {
	 const float _Speed = 30;
    const float _Power= 2;
    TinyBombStateMachine TBSM;

    public TinyBomb(TinyBombStateMachine _TBSM)
	{
        TBSM = _TBSM;
	}
    public float Speed
    {
        get { return _Speed; }
    }

    public float Power
    {
        get { return _Power; }
    }
    public void Move(GameObject tinyBomb)
    {
      //  Debug.Log("tiny bomb speed "+Speed);
        tinyBomb.GetComponent<Rigidbody2D>().velocity = new Vector2(Speed, 0);
    }


   
}
