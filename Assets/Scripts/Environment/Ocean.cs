﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ocean : MonoBehaviour {
    public float Density;
    public float BombaDensity;
    public Material PetrolMaterial;
    public Material WaterMaterial;
	
	// Update is called once per frame
	void Update () {
        //MakePetrol();
	}
    float polygonArea(List<Vector2> points, int numPoints) 
    { 
        float area = 0.0f;         // Accumulates area in the loop
        int j = numPoints-1;  // The last vertex is the 'previous' one to the first

        for (int i = 0; i < numPoints; i++)
        { 
            area = area +  (points[j].x + points[i].x) * (points[j].y - points[i].y); 
            j = i;  //j is previous vertex to i
        }
        return area/2;
    }

    void Start()
    {
        Density = 0.01577124f;
    }

    public void MakePetrol()
    {
       // transform.GetComponentInChildren<Water>().watermesh.GetComponent<Renderer>().material = PetrolMaterial;
    }

    public void MakeWater()
    {
       // transform.GetComponentInChildren<Water>().watermesh.GetComponent<Renderer>().material = WaterMaterial;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.GetComponent<Rigidbody2D>() && !other.GetComponent<Rigidbody2D>().isKinematic)
        { 
            GameObject floatingObject  = other.gameObject;
         
            float OverlapingArea = 0.0f;
            if ((CircleCollider2D)floatingObject.GetComponent<CircleCollider2D>()) { 

                float r = ((CircleCollider2D)floatingObject.GetComponent<CircleCollider2D>()).radius;
                  BombaDensity = floatingObject.GetComponent<Rigidbody2D>().mass / (Mathf.PI * r * r);
            
                float h = (((BoxCollider2D)GetComponent<BoxCollider2D>()).size.y + r)
                    - (Mathf.Abs(floatingObject.transform.position.y - ((BoxCollider2D)GetComponent<BoxCollider2D>()).offset.y));
                if (h < r)
                {
                    float cosTheta = h / r;
                    float a = 0.5f * Mathf.Acos(cosTheta) * r * r;
                   // Debug.Log("a = " + a);
                    OverlapingArea = a - 0.5f * r * r * Mathf.Sin(Mathf.Acos(cosTheta));
                }
                else if (h < 2 * r)
                {

                    float cosTheta = (2 * r - h) / r;
                    float a = 0.5f * Mathf.Acos(cosTheta) * r * r;
                 //   Debug.Log("a = " + a);
                    OverlapingArea = Mathf.PI * r * r - a - 0.5f * r * r * Mathf.Sin(Mathf.Acos(cosTheta));
                }
                else
                {
                    OverlapingArea = Mathf.PI * r * r;
                    //addForce = false;
                }

            
            

        
            }
            else if ((PolygonCollider2D)floatingObject.GetComponent<PolygonCollider2D>())
            {
                List<Vector2> points = new List<Vector2>();
            
                float waterSurface = gameObject.GetComponent<BoxCollider2D>().offset.y - gameObject.GetComponent<BoxCollider2D>().size.y;
                foreach (Vector2 point in ((PolygonCollider2D)floatingObject.GetComponent<PolygonCollider2D>()).points)
                {
                    if (point.y < waterSurface)
                    {
                        points.Add(point);
                    }
                    
                }
                OverlapingArea = polygonArea(points, points.Count);

            }

            else if ((BoxCollider2D)floatingObject.GetComponent<BoxCollider2D>())
            {
                float waterSurface = gameObject.GetComponent<BoxCollider2D>().offset.y - gameObject.GetComponent<BoxCollider2D>().size.y;
                if (waterSurface > ((BoxCollider2D)floatingObject.GetComponent<BoxCollider2D>()).offset.y - ((BoxCollider2D)floatingObject.GetComponent<BoxCollider2D>()).size.y)
                {
                    OverlapingArea = 4 * ((BoxCollider2D)floatingObject.GetComponent<BoxCollider2D>()).size.y * ((BoxCollider2D)floatingObject.GetComponent<BoxCollider2D>()).size.x;
                }
                else
                {
                    OverlapingArea = 2 * ((BoxCollider2D)floatingObject.GetComponent<BoxCollider2D>()).size.x * (((BoxCollider2D)floatingObject.GetComponent<BoxCollider2D>()).size.y + ((BoxCollider2D)floatingObject.GetComponent<BoxCollider2D>()).offset.y);
                }
            }


            float displacedMass = Density * OverlapingArea;
            Vector2 buoyancyForce = (displacedMass) * -new Vector2(0, -9.81f);

            if (other && other.GetComponent<Rigidbody2D>()) { other.GetComponent<Rigidbody2D>().AddForce(buoyancyForce, ForceMode2D.Force);
               // Debug.Log(other.name);
            }
            //other.attachedRigidbody.AddForce(buoyancyForce, ForceMode2D.Force);

        }

       // Debug.Log("added force");
        
    

    }
    void OnTriggerExit2D(Collider2D other)
    {

    }
}
