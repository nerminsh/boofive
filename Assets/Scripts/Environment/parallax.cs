﻿using UnityEngine;
using System.Collections;

public class parallax : MonoBehaviour
{

    public float Speed;
    public float resetDistance;
    public float initialDistance;
    public bool Active;
    public GameObject other;
    float otherSize;
    float mySize;
    public bool fakes = true;
    public float aianKan = 0;


    void Start()
    {
        otherSize = 0;
        for (int i = 0; i < other.transform.childCount; i++)
        {
            otherSize += other.transform.GetChild(i).gameObject.GetComponent<Renderer>().bounds.size.x;
        }
        otherSize += 0.5f * other.GetComponent<Renderer>().bounds.size.x;
        mySize = 0.5f * GetComponent<Renderer>().bounds.size.x;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (Active)
        {

            float move = Speed * Time.deltaTime;

            if (transform.position.x <= resetDistance)
            {
                if (!fakes)
                {
                    move = 0;
                }
                float Size = otherSize;

                transform.position = new Vector3(other.transform.position.x
                                                 - move
                                                +Size+
                                                 
                                                 + mySize
                                                 - aianKan
                                                 , transform.position.y, transform.position.z);
                //Active = false;

            }
            else
            {
                transform.Translate(Vector3.left * move, Space.World);
            }
        }
    }


}
