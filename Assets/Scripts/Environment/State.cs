﻿using UnityEngine;
using System.Collections;

public enum BombaState
{
    OnTheSurface,
    OnTheMiddle,
    OnTheBottom,
    petroleum
} 

public class State : MonoBehaviour {

    public BombaState bombaState;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Surface")
        {
            bombaState = BombaState.OnTheSurface;
        }
        else if (other.tag == "Inside")
        {
            bombaState = BombaState.OnTheMiddle;
        }
        else if (other.tag == "Deep")
        {
            bombaState = BombaState.OnTheBottom;            
        }
    }
}
