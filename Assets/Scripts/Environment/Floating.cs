﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Floating : MonoBehaviour {
    public float density;
    public float OverlapingArea = 0.0f;
    
    float polygonArea(List<Vector2> points, int numPoints)
    {
        float area = 0.0f;         // Accumulates area in the loop
        int j = numPoints - 1;  // The last vertex is the 'previous' one to the first

        for (int i = 0; i < numPoints; i++)
        {
            area = area + (points[j].x + points[i].x) * (points[j].y - points[i].y);
            j = i;  //j is previous vertex to i
        }
        return Mathf.Abs(area / 2);
    }

    void CalculateArea()
    {
        if ((CircleCollider2D)GetComponent<CircleCollider2D>())
        {

            float r = ((CircleCollider2D)GetComponent<CircleCollider2D>()).radius;
            OverlapingArea = Mathf.PI * r * r;
        }

        else if ((PolygonCollider2D)GetComponent<PolygonCollider2D>())
        {
            List<Vector2> points = new List<Vector2>();
            foreach (Vector2 point in ((PolygonCollider2D)GetComponent<PolygonCollider2D>()).points)
            {
                points.Add(point);
            }
            OverlapingArea = polygonArea(points, points.Count);

        }

        else if ((BoxCollider2D)GetComponent<BoxCollider2D>())
        {
            OverlapingArea = 4 * ((BoxCollider2D)GetComponent<BoxCollider2D>()).size.y * ((BoxCollider2D)GetComponent<BoxCollider2D>()).size.x;
        }
    }
    public void CalculateDensity()
    {
        CalculateArea();
       
        density = GetComponent<Rigidbody2D>().mass / OverlapingArea;
    }
    public void ResetMass(float den)
    {
        GetComponent<Rigidbody2D>().mass = den * OverlapingArea;
        density = den;
    }
    void Update()
    {
       // Debug.Log(rigidbody2D.velocity.y);
    }
}
