﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Float : MonoBehaviour {

    private int ColliderType = 0;
    private Collider2D coll;
    private BoxCollider2D Oceancoll;
    private float OverlapingArea = 0.0f;
    private Rigidbody2D rbody;
    public float Density = 0;
    private float OceanDensity = 0.01577124f;
	public int index = -1;
    public bool DoFloat = false;
    void Start () {

        ColliderType = (GetComponent<CircleCollider2D>()) ? 1 : (GetComponent<PolygonCollider2D>() ? 2 : 3);
        coll = GetComponent<Collider2D>();
        rbody = GetComponent<Rigidbody2D>();
        Density = GetComponent<Floating>().density;
        Oceancoll = GameObject.FindGameObjectWithTag("Ocean").GetComponent<BoxCollider2D>();
        StartCoroutine(AddForce());
    }

    float polygonArea(List<Vector2> points, int numPoints)
    {
        float area = 0.0f;
        int j = numPoints - 1;

        for (int i = 0; i < numPoints; i++)
        {
            area = area + (points[j].x + points[i].x) * (points[j].y - points[i].y);
            j = i;
        }
        return area / 2;
    }
    IEnumerator AddForce()
    {
        while (true) {
            if (DoFloat) { 
                GetOverlapingArea();
                float displacedMass = OceanDensity * OverlapingArea;
                Vector2 buoyancyForce = (displacedMass) * index *new Vector2(0, -9.81f);
                if(!float.IsNaN(buoyancyForce.x) && !float.IsNaN(buoyancyForce.y))
                    rbody.AddForce(buoyancyForce, ForceMode2D.Force);
          }
            yield return new WaitForFixedUpdate();
        }
          
    }
    void OnTriggerEnter2D(Collider2D Other)
    {
        if (Other.tag == "Ocean")
            DoFloat = true;
    }
    void OnTriggerExit2D(Collider2D Other)
    {
        if (Other.tag == "Ocean")
            DoFloat = false;
    }
    void GetOverlapingArea()
    {
        switch (ColliderType)
        {
            case 1:
                GetCircle();
                break;
            case 2:
                GetPolygon();
                break;
            case 3:
                GetBox();
                break;
            default:
                break;
        }
    }

    void GetCircle()
    {
        float r = ((CircleCollider2D)coll).radius;

        float h = (Oceancoll.size.y + r)
            - (Mathf.Abs(transform.position.y - (Oceancoll.offset.y)));
        if (h < r)
        {
            float cosTheta = h / r;
            float a = 0.5f * Mathf.Acos(cosTheta) * r * r;
            OverlapingArea = a - 0.5f * r * r * Mathf.Sin(Mathf.Acos(cosTheta));
        }
        else if (h < 2 * r)
        {

            float cosTheta = (2 * r - h) / r;
            float a = 0.5f * Mathf.Acos(cosTheta) * r * r;
            OverlapingArea = Mathf.PI * r * r - a - 0.5f * r * r * Mathf.Sin(Mathf.Acos(cosTheta));
        }
        else
        {
            OverlapingArea = Mathf.PI * r * r;
        }
    }
    void GetPolygon()
    {
        List<Vector2> points = new List<Vector2>();

        float waterSurface = Oceancoll.offset.y - Oceancoll.size.y;
        foreach (Vector2 point in ((PolygonCollider2D)coll).points)
        {
            if (point.y < waterSurface)
            {
                points.Add(point);
            }

        }
        OverlapingArea = polygonArea(points, points.Count);
    }
    void GetBox()
    {
        float waterSurface = Oceancoll.offset.y - Oceancoll.size.y;
        if (waterSurface > (((BoxCollider2D)coll).offset.y - ((BoxCollider2D)coll).size.y))
        {
            OverlapingArea = 4 * ((BoxCollider2D)coll).size.y * ((BoxCollider2D)coll).size.x;
        }
        else
        {
            OverlapingArea = 2 * ((BoxCollider2D)coll).size.x * (((BoxCollider2D)coll).size.y + ((BoxCollider2D)coll).offset.y);
        }
    }
}
