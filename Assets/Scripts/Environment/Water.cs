﻿using UnityEngine;
using System.Collections;

public class Water : MonoBehaviour {

    //SoundMan
    Sounds soundMan;
    
    LineRenderer Body;
    
	public float speed = 3;
    
	float[] xpositions;
    float[] ypositions;
    float[] velocities;
    float[] accelerations;

	ParticleSystem SplashParticleSystem;
    
	public float Left;
    public float Width;
    public float Top;
    public float Bottom;
	
    //GameObject[] colliders;
    
    public GameObject splash;

    public Material mat;

    public float springconstant = 0.02f;
    public float damping = 0.04f;
    public float spread = 0.05f;
    public float z_line = 0.0f;

    float baseheight;
    float left;
    float bottom;
    

    void Start()
    {
        soundMan = GameObject.Find("SoundMan").GetComponent<Sounds>();
		SplashParticleSystem = splash.GetComponent<ParticleSystem> ();
        SpawnWater(Left,Width,Top,Bottom);
    }

    
    public void Splash(float xpos, float velocity, bool sp)
    {
        //If the position is within the bounds of the water:
        if (xpos >= xpositions[0] && xpos <= xpositions[xpositions.Length-1])
        {
            //Offset the x position to be the distance from the left side
            xpos -= xpositions[0];

            //Find which spring we're touching
            int index = Mathf.RoundToInt((xpositions.Length-1)*(xpos / (xpositions[xpositions.Length-1] - xpositions[0])));

            //Add the velocity of the falling object to the spring
            velocities[index] += velocity;
            if (sp) { 
                //Set the lifetime of the particle system.
                float lifetime = 0.93f + Mathf.Abs(velocity)*0.07f;

                // Make splashing sound
                soundMan.Splashing(transform);
                
				SplashParticleSystem.startSpeed = 8+2*Mathf.Pow(Mathf.Abs(velocity),0.5f);
				SplashParticleSystem.startSpeed = 9 + 2 * Mathf.Pow(Mathf.Abs(velocity), 0.5f);
				SplashParticleSystem.startLifetime = lifetime;

                //Set the correct position of the particle system.
                Vector3 position = new Vector3(xpositions[index],ypositions[index]+ Random.Range (-1.0f,2.0f),5);

                //This line aims the splash towards the middle. Only use for small bodies of water:
                //Quaternion rotation = Quaternion.LookRotation(new Vector3(xpositions[Mathf.FloorToInt(xpositions.Length / 2)], baseheight + 8, 5) - position);
            
                //Create the splash and tell it to destroy itself.
                GameObject splish = Instantiate(splash,position,Quaternion.Euler(-90,0,0)) as GameObject;
//				splish.particleEmitter.localVelocity 
                Destroy(splish, lifetime+0.3f);
            }
        }
    }

    public void SpawnWater(float Left, float Width, float Top, float Bottom)
    {
        


        //Calculating the number of edges and nodes we have
        //int edgecount = Mathf.RoundToInt(Width) * 5;
        int edgecount =(int) Width * 2;
        int nodecount = edgecount + 1;
        
        //Add our line renderer and set it up:
        Body = gameObject.AddComponent<LineRenderer>();
        Body.material = mat;
        //Body.material.renderQueue = 1000;
        Body.SetVertexCount(nodecount);
        Body.SetWidth(0.06f, 0.06f);

        //Declare our physics arrays
        xpositions = new float[nodecount];
        ypositions = new float[nodecount];
        velocities = new float[nodecount];
        accelerations = new float[nodecount];
        
        //Declare our mesh arrays
       // meshobjects = new GameObject[edgecount];
       // meshes = new Mesh[edgecount];
        //colliders = new GameObject[edgecount];

        //Set our variables
        baseheight = Top;
        bottom = Bottom;
        left = Left;

        //For each node, set the line renderer and our physics arrays
        for (int i = 0; i < nodecount; i++)
        {
            ypositions[i] = Top;
            xpositions[i] = Left + Width * i / edgecount;
            Body.SetPosition(i, new Vector3(xpositions[i], Top, 1.5f));
            accelerations[i] = 0;
            velocities[i] = 0;
        }

        //Setting the meshes now:
//        for (int i = 0; i < edgecount; i++)
//        {
//            colliders[i] = new GameObject();
//            colliders[i].name = "Trigger";
//            colliders[i].AddComponent<BoxCollider2D>();
//            colliders[i].transform.parent = transform;
//
//            //Set the position and scale to the correct dimensions
//            colliders[i].transform.position = new Vector3(Left + Width * (i + 0.5f) / edgecount, Top - 0.05f, 0);
//            colliders[i].transform.localScale = new Vector3(Width / edgecount, 0.1f, 1);
//
//            //Add a WaterDetector and make sure they're triggers
//            colliders[i].GetComponent<BoxCollider2D>().isTrigger = true;
//            colliders[i].AddComponent<WaterDetector>();
//            //m = (Move)colliders[i].AddComponent<Move>();
//            //m.speed = 0.1f;
//            //colliders[i].rigidbody2D.isKinematic = true;
//
//
//        }

        
        
        
    }

    //Same as the code from in the meshes before, set the new mesh positions
    //void UpdateMeshes()
    //{
    //    for (int i = 0; i < meshes.Length; i++)
    //    {

    //        Vector3[] Vertices = new Vector3[4];
    //        Vertices[0] = new Vector3(xpositions[i], ypositions[i], z);
    //        Vertices[1] = new Vector3(xpositions[i+1], ypositions[i+1], z);
    //        Vertices[2] = new Vector3(xpositions[i], bottom, z);
    //        Vertices[3] = new Vector3(xpositions[i+1], bottom, z);

    //       // meshes[i].vertices = Vertices;
    //    }
    //}

    //Called regularly by Unity
    void FixedUpdate()
    {
        //Here we use the Euler method to handle all the physics of our springs:
        for (int i = 0; i < xpositions.Length ; i++)
        {
            float force = springconstant * (ypositions[i] - baseheight) + velocities[i]*damping ;
            accelerations[i] = -force;
            ypositions[i] += velocities[i];
            velocities[i] += accelerations[i];
            Body.SetPosition(i, new Vector3(xpositions[i] , ypositions[i], 1.5f));
        }

        //Now we store the difference in heights:
        float[] leftDeltas = new float[xpositions.Length];
        float[] rightDeltas = new float[xpositions.Length];

        //We make 8 small passes for fluidity:
        for (int j = 0; j < 8; j++)
        {
            for (int i = 0; i < xpositions.Length; i++)
            {
                //We check the heights of the nearby nodes, adjust velocities accordingly, record the height differences
                if (i > 0)
                {
                    leftDeltas[i] = spread * (ypositions[i] - ypositions[i-1]);
                    velocities[i - 1] += leftDeltas[i];
                }
                if (i < xpositions.Length - 1)
                {
                    rightDeltas[i] = spread * (ypositions[i] - ypositions[i + 1]);
                    velocities[i + 1] += rightDeltas[i];
                }
            }

            //Now we apply a difference in position
            for (int i = 0; i < xpositions.Length; i++)
            {
                if (i > 0)
                    ypositions[i-1] += leftDeltas[i];
                if (i < xpositions.Length - 1)
                    ypositions[i + 1] += rightDeltas[i];
            }
        }
        //Finally we update the meshes to reflect this
        //UpdateMeshes();
	}

    void OnTriggerStay2D(Collider2D Hit)
    {
        //Bonus exercise. Fill in your code here for making things float in your water.
        //You might want to even include a buoyancy constant unique to each object!
    }



}
