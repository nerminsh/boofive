﻿using UnityEngine;
using System.Collections;

public class particleEffectColor : MonoBehaviour {
	public Color color1;
	public Color color2;
	ParticleSystem ps;

	// Use this for initialization
	void Start () {
		ps = GetComponent<ParticleSystem> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		Color c = Color.Lerp (color1, color2, Time.deltaTime/5);
		ps.startColor = c;
	}
}
