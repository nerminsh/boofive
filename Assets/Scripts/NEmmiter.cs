﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NEmmiter : MonoBehaviour
{
    public int timeMax = 10;
    public float timeMin = 0.2f;
    int slicesCounter = 1;
    int slicesCount = 7;
    int PatternsCount = 0;
    int SubmarineType = 0;
    public GameObject[] GameObjects;
    public GameObject[] GameObjects1;
    public GameObject[] GameObjects2;
    public GameObject[] Coins;
    public GameObject[] powerUps;
    public GameObject[] Submarines;
    public GameObject submarine;
    public GameObject Bomba;
    Bomba bombaClass;
    bool sub = false;
    public float time;
    public float CoinsTime;
    public float SubmarinesTime;
    public float PowerUpTime;
    public int i = 0;
    public List<GameObject> Obs;

    // Use this for initialization
    void Start()
    {
        //time = Time.time;
        //CoinsTime = int.MaxValue;
        //SubmarinesTime = int.MaxValue;
        //PowerUpTime = int.MaxValue;
        //bombaClass = Bomba.GetComponent<Bomba>();
        //Reset();
        

    }
    public void Reset()
    {
      
        for (int k = 0; k < Game.Obstacles.Count; k++)
        {
            Destroy(Game.Obstacles[k]);
        }
        Game.Obstacles.Clear();
        time = Time.time + 2;
        CoinsTime = int.MaxValue;
        SubmarinesTime = int.MaxValue;
        PowerUpTime = int.MaxValue;
        bombaClass = Game.bom;
        slicesCounter = 0;
        if (submarine)submarine.GetComponent<Submarine>().finishedAmmo = true;
        i = 0;
        
        

    }

    // Update iub called once per frame
    //void Update () {

    //}
    void FixedUpdate()
    {
        if (Time.time >= time && Bomba)
        {

            switch (Bomba.GetComponent<State>().bombaState)
            {
                case BombaState.OnTheSurface:
                    {
                        GameObject Slice = (GameObject)Instantiate(GameObjects[Random.Range(0, GameObjects.Length)],
                        new Vector2(gameObject.transform.position.x, 8),
                        Quaternion.identity);
                        while (!Slice)
                        {
                            Slice = (GameObject)Instantiate(GameObjects[Random.Range(0, GameObjects.Length)],
                            new Vector2(gameObject.transform.position.x, 8),
                            Quaternion.identity);
                        }
                        Game.Obstacles.Add(Slice);
                        
                        bool emmitCoins = Random.Range(0, 10) <= 3;
                        if (emmitCoins)
                        {
								Game.Obstacles.Add((GameObject)Instantiate(Coins[Random.Range(Coins.Length -3, Coins.Length)],
                                new Vector2(gameObject.transform.position.x,
                                    -5),
                                    Quaternion.identity));
                        }
                    }
                    break;
                case BombaState.OnTheMiddle:
                    {
                        GameObject Slice = (GameObject)Instantiate(GameObjects1[Random.Range(0, GameObjects1.Length)],
                        new Vector2(gameObject.transform.position.x, 0),
                        Quaternion.identity);
                        while (!Slice)
                        {
                            Slice = (GameObject)Instantiate(GameObjects1[Random.Range(0, GameObjects1.Length)],
                            new Vector2(gameObject.transform.position.x, 0),
                            Quaternion.identity);
                        }
                        bool emmitCoins = Random.Range(0, 10) <= 3;
                        Game.Obstacles.Add(Slice);
                        if (emmitCoins)
                        {
                            Game.Obstacles.Add((GameObject)Instantiate(Coins[Random.Range(Coins.Length -3, Coins.Length)],
                                new Vector2(gameObject.transform.position.x,
                                    -9),
                                    Quaternion.identity));
                        }
                    }
                    break;
                case BombaState.OnTheBottom:
                    {
                        GameObject Slice = (GameObject)Instantiate(GameObjects2[Random.Range(0, GameObjects2.Length)],
                new Vector2(gameObject.transform.position.x, -6.5f),
                Quaternion.identity);
                        while (!Slice)
                        {
                            Slice = (GameObject)Instantiate(GameObjects2[Random.Range(0, GameObjects2.Length)],
                            new Vector2(gameObject.transform.position.x, -5),
                            Quaternion.identity);
                        }
                        bool emmitCoins = Random.Range(0, 10) <= 3;
                        Game.Obstacles.Add(Slice);
                        if (emmitCoins)
                        {
                            Game.Obstacles.Add((GameObject)Instantiate(Coins[Random.Range(Coins.Length -3, Coins.Length)],
                                new Vector2(gameObject.transform.position.x,
                                    2),
                                    Quaternion.identity));
                        }
                    }
                    break;
                default:
                    break;
            }
            
                slicesCounter++;
            

            if (slicesCounter == slicesCount + 1)
            {
                slicesCounter = 0;
                PatternsCount++;
                if (PatternsCount == 2)
                {
                    PatternsCount = 0;
                    CoinsTime = int.MaxValue;
                    SubmarinesTime = Time.time + timeMax / 2;
                    time = int.MaxValue;

                }
                else
                {
                    time = Time.time + timeMax;
                    CoinsTime = Time.time + (timeMax / 2);
                    PowerUpTime = Time.time + (timeMax/2);
                    if (Game.METER < 2000)
                    {
                        slicesCount = Easy();
                    }
                    else if (Game.METER < 4000)
                    {
                        slicesCount = Medium();
                    }
                    else
                    {
                        slicesCount = Hard();
                    }
                }

            }
            else
            {
                time = Time.time + timeMin;
            }

        }
        if (Time.time >= SubmarinesTime && Time.time < time)
        {
            sub = true;
            SubmarinesTime = int.MaxValue;
            Debug.Log("Coins " + CoinsTime);

            submarine = (GameObject)Instantiate(Submarines[i++],
               new Vector2(gameObject.transform.position.x, 0),
               Quaternion.identity);
            Game.Obstacles.Add(submarine);
            if (i == Submarines.Length)
            {
                i = 0;
            }
        }
        if (Time.time >= CoinsTime && Time.time < time)
        {
            //bool emmitCoins = Random.Range(0, 10) <= 7;
            //Debug.Log("Coins " + CoinsTime);
            if (true)
            {
                Game.Obstacles.Add((GameObject)Instantiate(Coins[Random.Range(0, Coins.Length-3)],
                    new Vector2(gameObject.transform.position.x,
                        Random.Range(-5, 3)),
                        Quaternion.identity));
            }
            CoinsTime = int.MaxValue;
        }
        if (Time.time >= PowerUpTime && Time.time < time)
        {
            //Debug.Log("PowerUp "+PowerUpTime);
            if (bombaClass.hasPowerUp==false)
            {
                Game.Obstacles.Add((GameObject)Instantiate(powerUps[Random.Range(0, powerUps.Length)],
                    new Vector2(gameObject.transform.position.x,
                        Random.Range(-5, 3)),
                        Quaternion.identity));
            }
            PowerUpTime = int.MaxValue;
        }
        if (submarine && submarine.GetComponent<Submarine>().finishedAmmo && sub)
        {
            time = Time.time + timeMax;
            sub = false;
        }
        //Debug.Log(slicesCounter++);

        //GameObject obj = (GameObject)Instantiate(GameObjects[Random.Range(0, 
        //    GameObjects.Length)],new Vector2(gameObject.transform.position.x,0), Quaternion.identity);
        //if (slicesCounter%3==0){
        //    bool emmitCoins = Random.Range(0, 10) <= 7;
        //    if (emmitCoins)
        //    {
        //        Instantiate(Coins[Random.Range(0,GameObjects.Length)],
        //            new Vector2(gameObject.transform.position.x,
        //                Random.Range(-2, 2)),
        //                Quaternion.identity);
        //    }
        //    Invoke("Emmit", timeMax);
        //}
        //else
        //    Invoke("Emmit", timeMin);

    }
    int Easy()
    {
        int randNum = Random.Range(1, 10);
        slicesCounter = 0;
        if (randNum <= 7)
        {
            return 10;
        }
        else if (randNum <= 9)
        {
            return 20;
        }
        else
        {
            return 30;
        }
    }
    int Medium()
    {
        int randNum = Random.Range(1, 10);
        slicesCounter = 0;
        if (randNum <= 7)
        {
            return 20;
        }
        else if (randNum <= 9)
        {
            return 10;
        }
        else
        {
            return 30;
        }
    }
    int Hard()
    {
        int randNum = Random.Range(1, 10);
        slicesCounter = 0;
        if (randNum <= 7)
        {
            return 30;
        }
        else if (randNum <= 9)
        {
            return 20;
        }
        else
        {
            return 10;
        }

    }
}
