﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour
{
	public GameObject shiny;

    Sounds soundMan;
    public float Speed;
    Transform Target;
    Rigidbody2D rigTarget;
    Bomba b;
    Rigidbody2D rigBody;
    // Use this for initialization
    void Start()
    {
        soundMan = GameObject.FindGameObjectWithTag("SoundMan").GetComponent<Sounds>();

        Speed = 20;
        Target = Game.bomba.transform;
        rigTarget = Game.bomba.GetComponent<Rigidbody2D>();

        b = Game.bom;
        rigBody = GetComponent<Rigidbody2D>();

     //   gameObject.AddComponent("Move");

    }

    // Update is called once per frame
    void Update()
    {
        if (Game.bomba&&Game.bom.isMagnet)
        {
            float distance = Vector2.Distance(rigTarget.position, rigBody.position);
            if (distance <= 10)
            {
                rigBody.velocity = Truncate(Seek(Target) + rigBody.velocity, Speed);
            }
        }
    }
    public Vector2 Seek(Transform target)
    {
        Vector2 desiredVelocity = target.position - transform.position;
        desiredVelocity = desiredVelocity.normalized * Speed;
        Vector2 steeringForce = desiredVelocity - rigBody.velocity;
        return steeringForce;

    }

    public Vector2 Truncate(Vector2 vector, float speed)
    {
        if (vector.magnitude > speed)
        {
            return vector.normalized * speed;
        }
        else
            return vector;

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bomba")
        {
			if(shiny){
				GameObject s = (GameObject)Instantiate(shiny,transform.position,Quaternion.identity);

			}
            soundMan.Coin();
            gameObject.SetActive(false);
            Destroy(gameObject);
            Game.COINS++;
        }
    }
}
