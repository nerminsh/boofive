﻿using UnityEngine;
using System.Collections;

public class Undest : MonoBehaviour {
	public GameObject particleEffect;
    public int numOfTinyBombs;
    int countTiny;
    public GameObject exp;
	// Use this for initialization
	void Start () {
        countTiny = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerStay2D(Collider2D other)
    {

        if (other.tag == "TinyBomb")
        {
            if (particleEffect)
            {
                countTiny++;
                GameObject g = (GameObject)Instantiate(particleEffect, new Vector2(other.transform.position.x + other.GetComponent<Renderer>().bounds.size.x / 2, other.transform.position.y), Quaternion.identity);
                g.transform.parent = transform;
            }
            Destroy(other.gameObject);

            if (countTiny==numOfTinyBombs)
            {
                Destroy(gameObject);
                if (exp)
                {
                    Game.Obstacles.Add((GameObject)Instantiate(exp, transform.position, Quaternion.identity));
                }
            }
        }
        
    }
}
