﻿using UnityEngine;
using System.Collections;

public class Diving  {
    public static void dive(GameObject gameObject,float force)
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, force), ForceMode2D.Force);
    }
}
