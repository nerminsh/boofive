﻿using UnityEngine;
using System.Collections;

public class Shoot{
    static SpriteRenderer renderer;
    public void Start()
    {
        renderer = Game.bomba.GetComponent<SpriteRenderer>();
    }
    public static void shoot(GameObject shooter,GameObject bullet)
    {
        Game.Obstacles.Add((GameObject)GameObject.Instantiate(bullet,
            new Vector3(shooter.GetComponent<SpriteRenderer>().bounds.size.x / 2 +
            shooter.transform.position.x + bullet.GetComponent<Renderer>().bounds.size.x / 2, 
            shooter.transform.position.y, shooter.transform.position.z), Quaternion.identity));
    }
}
