﻿using UnityEngine;
using System.Collections;

public class Bomba : MonoBehaviour
{

    // Added by Arsh :D
    public bool flipControls = false;
    Sounds soundman;
    SpriteRenderer red;
    Rigidbody2D rigidBody;
    public GameObject Red;
	bool sh = false;
	bool tap = false;
	bool hold = false;
	Float f;  
    // EOA = End of Arsh

    BombaUpgradesStateMachine bombaUpgradeMachine;
    PowerUpsStateMachine powerUpStateMachine;
    public static bool canShoot;
    public  bool hasPowerUp;
    public bool isProtected;
    public bool isMagnet;
    int hitCounter = 0;

    public static float heatingRatio;

    public static float standardHeatingRatio;

    public float DownVel = 7f;
    public float DownVelDec = 0.2f;
    public float DownMass = 100;

    public float Drag = 1;
    public float Mass = 0.5f;
    BombaUpgradesState currentUpgradeState;

    PowerUpState currentPowerUp;
    public int HitCounter
    {
        get { return hitCounter; }
        set { hitCounter = value; }
    }
    public BombaUpgradesStateMachine BombaUpgradeMachine1
    {
        get { 
            return bombaUpgradeMachine; 
            }
    }
    public PowerUpState CurrentPowerUp
    {
        get
        {
           return powerUpStateMachine.getCurrentPowerUpState(); 
        }
    }

    public PowerUpsStateMachine PowerUpStateMachine
    {
        get {
            return powerUpStateMachine;
        }
    }
    public BombaUpgradesState CurrentUpgradeState
    {
        get { 
            return bombaUpgradeMachine.getCurrentUpgradeState();
            }
    }
    float MaxNumberOfTinyBombs;

    public float MaxNumberOfTinyBombs1
    {
        get { return MaxNumberOfTinyBombs; }
        set { MaxNumberOfTinyBombs = value; }
    }
    float TinyBombPower;

    public float TinyBombPower1
    {
        get { return TinyBombPower; }
        set { TinyBombPower = value; }
    }
    float CoolDown;

    public float CoolDown1
    {
        get { return CoolDown; }
        set { CoolDown = value; }
    }
    public GameObject tinyBombObject;
    // Use this for initialization
    void Start()
    {
		f = GetComponent<Float> ();
        soundman = GameObject.Find("SoundMan").GetComponent<Sounds>();
        canShoot = true;
        isMagnet = false;
        isProtected = false;
        hasPowerUp = false;
        hitCounter = 0;
        heatingRatio=0f;
        bombaUpgradeMachine = new BombaUpgradesStateMachine();
        powerUpStateMachine = new PowerUpsStateMachine();
        currentUpgradeState = bombaUpgradeMachine.BombaDefault;
        currentPowerUp = powerUpStateMachine.NoPowerUP;
        MaxNumberOfTinyBombs = currentUpgradeState.MaxNumberOfTinyBombs;
        TinyBombPower = currentUpgradeState.TinyBombPower;
        CoolDown = currentUpgradeState.CoolDown;
        standardHeatingRatio = 1 / currentUpgradeState.MaxNumberOfTinyBombs;
        red = Red.GetComponent<SpriteRenderer>();
        rigidBody = GetComponent<Rigidbody2D>();


       // Debug.Log("MOVE" + this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        sh = Input.GetKeyUp(KeyCode.RightControl);
        tap = Input.GetKeyDown(KeyCode.Space);
        hold = Input.GetKey(KeyCode.Space);
        red.color = new Color(1, 1, 1, heatingRatio);
        Touch[] touches = Input.touches;
        for (int i = 0; i < Input.touchCount; i++)
        {
            sh = sh || (flipControls && touches[i].phase == TouchPhase.Began && touches[i].position.x < Screen.width / 2)
                || (touches[i].phase == TouchPhase.Began && touches[i].position.x > Screen.width / 2);
            tap = tap || (flipControls && (touches[i].phase == TouchPhase.Began && touches[i].position.x > Screen.width / 2)) ||
                (touches[i].phase == TouchPhase.Began && touches[i].position.x < Screen.width / 2);
            hold = hold || (flipControls && ((touches[i].phase == TouchPhase.Stationary || touches[i].phase == TouchPhase.Moved) && touches[i].position.x > Screen.width / 2)) ||
                ((touches[i].phase == TouchPhase.Stationary || touches[i].phase == TouchPhase.Moved) && touches[i].position.x < Screen.width / 2);
        }
        if(sh){
            shoot();
        }
    }
	void FixedUpdate(){
		if (!f.DoFloat) 
		{
			if(rigidBody.velocity.y > 0){
				rigidBody.drag = 2.5f;
			}
			else
				rigidBody.drag = 0;
			
		}
		 
		else {
			rigidBody.drag = Drag;
		}
		if (tap && !(transform.position.y > 11))
		{
			//f.index = 1;
			//rigidBody.mass = DownMass;
			//rigidBody.velocity = new Vector2(rigidBody.velocity.x, DownVel);
			//rigidBody.velocity = new Vector2(rigidBody.velocity.x, 
			 //                                (DownVel * Time.fixedTime));
			rigidBody.AddForce(rigidBody.mass * 15 * new Vector2(0, -9.81f), ForceMode2D.Force);
			

		}
		else if(hold && !(transform.position.y > 11))
		{
			//f.index = 1;
			//rigidBody.mass = DownMass;
			//rigidBody.velocity = new Vector2(rigidBody.velocity.x,  
			 //                                (DownVel * Time.fixedTime));
			rigidBody.AddForce(rigidBody.mass *  6 * new Vector2(0, -9.81f), ForceMode2D.Force);
		}
		else if (tap && !(transform.position.y <= 11))
		{
			//f.index = 1;
			//rigidBody.mass = DownMass;
			//rigidBody.velocity = new Vector2(rigidBody.velocity.x, DownVel);
			//rigidBody.velocity = new Vector2(rigidBody.velocity.x, 
			//                                (DownVel * Time.fixedTime));
			rigidBody.AddForce(rigidBody.mass * 6 * new Vector2(0, -9.81f), ForceMode2D.Force);
			
			
		}
		else if(hold && !(transform.position.y <= 11))
		{
			//f.index = 1;
			//rigidBody.mass = DownMass;
			//rigidBody.velocity = new Vector2(rigidBody.velocity.x,  
			//                                (DownVel * Time.fixedTime));
			rigidBody.AddForce(rigidBody.mass *  3 * new Vector2(0, -9.81f), ForceMode2D.Force);
		}
		//-10.92
		//	if(transform.position.y
//		else if (transform.position.y > 11) {
//			rigidBody.mass = DownMass;
//			if(rigidBody.velocity.y > 0){
//				rigidBody.drag = 2.5f;
//			}
//			else
//				rigidBody.drag = 0;
//				
//		}
//		else
//		{
//			f.index = -1;
//			
//		}
	}
    void move()
    {
        //currentUpgradeState.dive(bomba.gameObject, -tabForce);
    }
    void shoot()
    {
        if (canShoot)
        {
            currentUpgradeState.shoot(this.gameObject, tinyBombObject);
            soundman.Shooting(transform);
            if (UIManager.gameR && UIManager.gameL)
            {
                increaseHeat();
            }
        }
    }

    void increaseHeat()
    {
        hitCounter++;
        heatingRatio = hitCounter / MaxNumberOfTinyBombs;
        if (heatingRatio > 1)
        {
            Game.BombaDied = true;
        }
        decreeasHeating();
        
    }
    
    public void decreeasHeating()
    {
        StartCoroutine(decreaseHeat());
    }
    IEnumerator decreaseHeat()
    {
        if (hitCounter > 0)
        {
            yield return new WaitForSeconds(currentUpgradeState.CoolDown);
            hitCounter--;
            heatingRatio = hitCounter / MaxNumberOfTinyBombs;
        }
        else
        {
            hitCounter = 0;
            heatingRatio = 0;
        }
           
    }
    public void OverHeat()
    {
      // hitCounter =(int) MaxNumberOfTinyBombs;

        heatingRatio += 0.5f;
        hitCounter += (int)(heatingRatio * MaxNumberOfTinyBombs);
        if (heatingRatio > 1)
        {
            Game.BombaDied = true;
            //Application.LoadLevel(0);
        }
        StartCoroutine(decreaseHeatLeave());
    }
    IEnumerator decreaseHeatLeave()
    {
    while (hitCounter>0)
	{
        yield return new WaitForSeconds(currentUpgradeState.CoolDown);
        hitCounter--;
        heatingRatio = hitCounter / MaxNumberOfTinyBombs;
	}
           
    }
    public void BombaDying()
    {
       
    }
}
