﻿using UnityEngine;
using System.Collections;

public class TinyBombs : MonoBehaviour {
    TinyBombStateMachine tinyBombMachine;
    TinyBombState currentTinyBombState;
    GameObject bomba;

    float Speed;
    float Power;
	// Use this for initialization
	void Start () {
        tinyBombMachine = new TinyBombStateMachine();
        currentTinyBombState = tinyBombMachine.getCurrentTinyBombState();
        bomba = Game.bomba;
        Speed = currentTinyBombState.Speed;
        Power = currentTinyBombState.Power;
	}
	
	// Update is called once per frame
	void Update () {

        rotate();
        move();
        
	}

    private void rotate()
    {
        transform.Rotate(0, 0,10f);
    }
    void move()

    {
        //rigidbody2D.AddForce(new Vector2(2, 0), ForceMode2D.Impulse);

//Debug.Log("bboomove");
        StartCoroutine(MoveTinyBomb());
          //  transform.position = new Vector2(transform.position.x, bomba.transform.position.y);
    }
    IEnumerator MoveTinyBomb()
    {

         currentTinyBombState.Move(this.gameObject);
      //  transform.position = new Vector2(transform.position.x, bomba.transform.position.y);
        yield return new WaitForSeconds(0.01f);


        transform.position = new Vector2(transform.position.x, transform.position.y);

    }
}
