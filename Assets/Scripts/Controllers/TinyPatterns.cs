﻿using UnityEngine;
using System.Collections;

public class TinyPatterns : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 1f)
        {
            rotate();
        }
    }

    private void rotate()
    {
        transform.Rotate(0, 0, 2f);
    }
}
