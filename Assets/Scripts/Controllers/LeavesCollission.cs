﻿using UnityEngine;
using System.Collections;

public class LeavesCollission : MonoBehaviour {
	public GameObject ps;
	// Use this for initialization
    void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.gameObject.CompareTag("Bomba"))
        {
            try
            {
                 Bomba b = Game.bom;
                

                b.OverHeat();
            }
            catch (System.Exception)
            {
               
            }
          
        }
        if (col.gameObject.CompareTag("TinyBomb"))
        {
			if(ps){
				Instantiate(ps,transform.position,Quaternion.identity);
			}
            Destroy(gameObject);
            Destroy(col);
        }
        //}
    }
}
