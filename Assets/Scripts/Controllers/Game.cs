﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour {

    public static int COINS;
      

    public static int METER;
    int allCoins = 0;

    public static GameObject bomba;
    public static Transform bombaTransform;
    public static Vector2 bombaStartPosition;
    public static bool BombaDied = false;
    public static List<GameObject> Obstacles = new List<GameObject>();
    static bool getCoins = false;
    static bool getScore = false;

   public static Bomba bom;
   
    // Use this for initialization
    void Awake()
    {
        getScore = false;
        getCoins = false;
        COINS = 0;
        METER = 0;
        bomba = GameObject.FindGameObjectWithTag("Bomba");
        bombaTransform = bomba.transform;
        bom = bomba.GetComponent<Bomba>();
        bombaStartPosition = new Vector2(bomba.transform.position.x, 0);

    }
    void Start () {
        
        StartCoroutine(IncreaseMeter());
	}
    void Update()
    {
        if (BombaDied)
        {
          bom.PowerUpStateMachine.defaultState();
          bom.PowerUpStateMachine.getCurrentPowerUpState().PowerBehavior(bom);

            //Debug.Log("power Up "+bomba.GetComponent<Bomba>().PowerUpStateMachine.getCurrentPowerUpState());
            
           bomba.GetComponent<Bomba>().HitCounter = 0;
           Bomba.heatingRatio = 0;

            SaveHighScore();
            SaveCoins();
        }
    }
	// Update is called once per frame
    IEnumerator IncreaseMeter()
    {
        while (true)
        {
            yield return new WaitForSeconds(.2f);
            METER++;
        }
    }
   public static void SaveHighScore()
    {
        int m = PlayerPrefs.GetInt("HighScore");
        if (m<METER && getScore==false)
        {
            PlayerPrefs.SetInt("HighScore",METER);
            getScore = true;
        }
    }
   public static void SaveCoins()
   {
       
      int c= PlayerPrefs.GetInt("allCoins");
      if (getCoins==false)
      {
          PlayerPrefs.SetInt("allCoins", c + COINS);
          c = 0;
          getCoins = true;
      }
     
   }
   public static int LoadHighScore()
    {
      return  PlayerPrefs.GetInt("HighScore",METER);
    }
      public static int LoadCoins()
    {
        return PlayerPrefs.GetInt("allCoins");
    }
}
