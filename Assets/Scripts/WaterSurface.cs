﻿using UnityEngine;
using System.Collections;

public class WaterSurface : MonoBehaviour {
    public static bool ABOVE_WATER = false;
    public bool abovewater = false;
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Bomba")
        {
			ABOVE_WATER = !ABOVE_WATER;
			abovewater = ABOVE_WATER;

        }
    }
}
