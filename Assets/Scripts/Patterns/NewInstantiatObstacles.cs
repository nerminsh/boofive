﻿using UnityEngine;
using System.Collections;

public class NewInstantiatObstacles : MonoBehaviour {
    Transform Above;
    Transform Inside;
    Transform Deep;
    public GameObject[] obstacles;
    GameObject ground;
	// Use this for initialization
	void Start () {
        Above = GameObject.FindGameObjectWithTag("Above").transform;
        Inside = GameObject.FindGameObjectWithTag("Insider").transform;
        Deep = GameObject.FindGameObjectWithTag("Deep").transform;

	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bomba")
        {
            
        }
    }
    void instantiatObstacle(GameObject[] obses)
    {
        System.Random r = new System.Random();
        int obs = r.Next(obses.Length);
        GameObject obj = (GameObject)Instantiate(obses[obs], transform.position, Quaternion.identity);
        while (!obj)
        {
            obj = (GameObject)Instantiate(obses[obs], transform.position, Quaternion.identity);
        }
 
        obj.transform.parent = this.transform.parent;
     

    }
}
