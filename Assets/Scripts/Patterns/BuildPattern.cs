﻿using UnityEngine;
using System.Collections;
using System;

public class BuildPattern : MonoBehaviour {
    public GameObject[] deep;
    public GameObject[] inside;
    public GameObject[] surface;
    GameObject surfaceObject;
    GameObject insideObject;
    GameObject deepObject;


	// Use this for initialization
	void Start () {
	surfaceObject=GameObject.FindGameObjectWithTag("Surface");
    insideObject=GameObject.FindGameObjectWithTag("Inside");
    deepObject=GameObject.FindGameObjectWithTag("Deep");
    instantiatObstacle(deep, deepObject);

	}
	
	// Update is called once per frame
	void Update () {
	}
    void instantiatObstacle(GameObject[] obstacles,GameObject obj)
    {
        System.Random r = new System.Random();
        Transform t = obj.transform;
        Debug.Log("arr " + obstacles.Length);
        int obs = r.Next(0, obstacles.Length);
        Debug.Log("t  " + t.position);
        obj = (GameObject)Instantiate(obstacles[obs],new Vector2(t.position.x,t.position.y), Quaternion.identity);
        obj.transform.position = t.position;
          
        
    }
}
