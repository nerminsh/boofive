﻿using UnityEngine;
using System.Collections;

public class InstantiatObstacle : MonoBehaviour {
    public GameObject[] obstacles;
    GameObject ground;
	// Use this for initialization
	void Start () {
      //  Debug.Log("obs11 "+obstacles.Length);
    instantiatObstacle(obstacles);
  //  ground = GameObject.FindGameObjectWithTag("Layer1");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void instantiatObstacle(GameObject[] obses)
    {
        //Debug.Log("obs22 " + obses.Length);
        System.Random r = new System.Random();
        int obs = r.Next(obses.Length);
       // Debug.Log("Random "+ obs);
        GameObject obj= (GameObject)Instantiate(obses[obs],transform.position, Quaternion.identity);
        while(!obj){
            obj = (GameObject)Instantiate(obses[obs], transform.position, Quaternion.identity);
        }
         //obj.transform.renderer.bounds.size
         //transform.FindChild();
        obj.transform.parent = transform;
       //Debug.Log( obj.GetComponentInChildren<Collider2D>().bounds.size);
       // obj.transform.FindChild("")

    }
}
