﻿using UnityEngine;
using System.Collections;

public class SheildObject : MonoBehaviour {
    Bomba b;
    Shield shield2;
    public GameObject shieldProtector;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bomba")
        {
            b = Game.bom;
            shield2 =shieldProtector.GetComponent<Shield>();
            b.hasPowerUp = true;
            b.isProtected = true;
            b.PowerUpStateMachine.setPowerUP(shield2);
            Instantiate(shieldProtector, new Vector2(other.transform.position.x, other.transform.position.y),
                 Quaternion.identity);
              Destroy(this.gameObject);

        }
    }
}
