﻿using UnityEngine;
using System.Collections;

public class UnlimitedTinyBombs :MonoBehaviour, PowerUpState {
    //const float numberOfTinyBommb = 1000;
     public const int time = 20;
    PowerUpsStateMachine PUSM;

    public UnlimitedTinyBombs(PowerUpsStateMachine _PUSM)
    {
        PUSM = _PUSM;
    }
    public int GetTime()
    {
        return time;
    }
    public void PowerBehavior(Bomba b)
    {
        b.MaxNumberOfTinyBombs1 = 1000;
        PUSM= b.PowerUpStateMachine;
        StartCoroutine("PowerUpState.SetBehavior", b);
    }

    IEnumerator PowerUpState.SetBehavior(Bomba b)
    {
        
        yield return new WaitForSeconds(time);
        PUSM.defaultState();
        PUSM.getCurrentPowerUpState().PowerBehavior(b);
    }
    public override string ToString()
    {
        return "UnlimitedTinyBombs Object";
    }

}
