﻿using UnityEngine;
using System.Collections;

public class NoPowerUP : MonoBehaviour, PowerUpState
{
    public const int time = 0;
    PowerUpsStateMachine PUSM;
    public NoPowerUP(PowerUpsStateMachine _PUSM)
    {
        PUSM = _PUSM;
    }
    public void PowerBehavior(Bomba b)
    {
        

        PUSM = b.PowerUpStateMachine;
        StartCoroutine("PowerUpState.SetBehavior", b);
    }
    public int GetTime()
    {
        return time;
    }
    IEnumerator PowerUpState.SetBehavior(Bomba b)
    {
        b.HitCounter = 0;
        b.CoolDown1 = b.CurrentUpgradeState.CoolDown;
        b.MaxNumberOfTinyBombs1 = b.CurrentUpgradeState.MaxNumberOfTinyBombs;
        b.TinyBombPower1 = b.CurrentUpgradeState.TinyBombPower;
        Destroy(b.gameObject.GetComponent<Magnet>());
        b.hasPowerUp = false;
        b.isMagnet = false;
        b.isProtected = false;
        yield return null;
    }
}
