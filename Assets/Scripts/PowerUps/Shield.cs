﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour, PowerUpState
{
    public const int time = 0;
    PowerUpsStateMachine PUSM;
    Bomba bo;
    GameObject bomba;

    public Shield(PowerUpsStateMachine _PUSM)
    {
        PUSM = _PUSM;
    }
    public int GetTime()
    {
        return time;
    }
    void Start()
    {
        bomba = Game.bomba;
        bo = Game.bom;
        transform.parent = bomba.transform;
   
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Obstacle" || other.gameObject.tag == "Wing" || other.gameObject.tag == "Leave")
        {
            Destroy(other.gameObject);
            PowerBehavior(bo);
        }
    }
    public void PowerBehavior(Bomba b)
    {
        StartCoroutine("PowerUpState.SetBehavior", bo);
    }

     IEnumerator SetBehavior(Bomba b)
    {  
        yield return new WaitForEndOfFrame();
        PUSM = b.PowerUpStateMachine;  
        PUSM.defaultState();
        Destroy(gameObject);
    }


     IEnumerator PowerUpState.SetBehavior(Bomba b)
     {
         yield return new WaitForEndOfFrame();
         PUSM = b.PowerUpStateMachine;
         PUSM.defaultState();
         Destroy(gameObject);
     }
}
