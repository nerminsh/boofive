﻿using UnityEngine;
using System.Collections;

public class MagnetObject : MonoBehaviour {
    public GameObject magnetPower;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bomba")
        {
            Instantiate(magnetPower, new Vector2(other.transform.position.x, other.transform.position.y),
              Quaternion.identity);
            Destroy(gameObject,1);
        }
    }
}
