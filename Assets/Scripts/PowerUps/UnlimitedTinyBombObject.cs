﻿using UnityEngine;
using System.Collections;

public class UnlimitedTinyBombObject : MonoBehaviour {
    Bomba b;
    UnlimitedTinyBombs unlimitedTinyBombs;
   
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bomba")
        {
        
             b = Game.bom ;
             b.hasPowerUp = true;
             unlimitedTinyBombs = gameObject.AddComponent(typeof(UnlimitedTinyBombs)) as UnlimitedTinyBombs;
             b.PowerUpStateMachine.setPowerUP(unlimitedTinyBombs);
             unlimitedTinyBombs.PowerBehavior(b);
             this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x - 100, this.gameObject.transform.position.y);
             Destroy(this.gameObject,30);
        }
    }
}
