﻿using UnityEngine;
using System.Collections;

public class Magnet : MonoBehaviour,PowerUpState {
    public const int time = 20;
    PowerUpsStateMachine PUSM;


     public Magnet(PowerUpsStateMachine _PUSM)
    {
        PUSM = _PUSM;
    }
     void Start()
     {
         
         transform.parent = Game.bomba.transform;
         PowerBehavior(Game.bom);
     }
     public int GetTime()
     {
         return time;
     }
    public void PowerBehavior(Bomba b)
    {
        Debug.Log(b);
        Debug.Log("PowerBehavior");
      
        StartCoroutine("PowerUpState.SetBehavior", b);
    }

    IEnumerator PowerUpState.SetBehavior(Bomba b)
    {
        Debug.Log("before yeild");

        b.hasPowerUp = true;
        b.isMagnet = true;
        b.PowerUpStateMachine.setPowerUP(this);
        yield return new WaitForSeconds(time);
        Debug.Log("after yeild"+Game.bomba.transform.childCount);
        PUSM = b.PowerUpStateMachine;
      Destroy(Game.bomba.transform.GetChild(1).gameObject);
 
        PUSM.defaultState();
        PUSM.getCurrentPowerUpState().PowerBehavior(b);
    }
    public override string ToString()
    {
         
        return "Magnet Object";
    }

}
