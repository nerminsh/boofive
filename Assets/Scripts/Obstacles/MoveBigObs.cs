﻿using UnityEngine;
using System.Collections;

public class MoveBigObs : MonoBehaviour {

    public float speed = -1f;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.timeScale == 1f)
        {
            transform.position = new Vector3((speed / 1000) + transform.position.x, transform.position.y, transform.position.z);    
        }
	}
}
