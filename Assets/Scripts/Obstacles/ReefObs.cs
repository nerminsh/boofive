﻿using UnityEngine;
using System.Collections;

public class ReefObs : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnCollisionEnter2D(Collision2D col)
    {
        // InDestructable Obj must be kinematic or handle the force applied
        //if (!gameObject.CompareTag("InD"))
        //{
        if (col.gameObject.CompareTag("TinyBomb"))
        {
            //Debug.Log("Destroyed");
            //if (!aboveMe)
            //{
            //    Destroy(gameObject);
            //}
            Destroy(gameObject);
            Destroy(col.gameObject);
        }
        //}
    }
}
