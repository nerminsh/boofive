﻿using UnityEngine;
using System.Collections;

public class Sounds : MonoBehaviour {

    GameObject bomba;

    public bool sound = true;
    public bool music = true;
    public GameObject splash;
    public AudioClip shoot;
    public AudioClip dead;
    public AudioClip coin;

    [Range(0.1f, 1.0f)]
    public float vol;
	private AudioSource BGvol;

	// Use this for initialization
	void Start () {
        //DontDestroyOnLoad(gameObject);
        bomba = GameObject.FindGameObjectWithTag("Bomba");
		 BGvol = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!music)
        {
			BGvol.volume = 0f;
        }
        else
        {
			BGvol.volume = vol;
        }

        if (Game.BombaDied)
        {
            //print("[SoundMan] Couldn't find Bomba! or She's Dead :'(");
            Dead();
            //bomba = GameObject.FindGameObjectWithTag("Bomba");
        }
	}

    public void Shooting(Transform loc)
    {
        if (sound)
        {
            AudioSource.PlayClipAtPoint(shoot, loc.position);
            // print("[SoundMan] Shooting Sound!");
        }
    }

    public void Splashing(Transform loc)
    {
        if (sound)
        {
            GameObject s = Instantiate(splash, bomba.transform.position, Quaternion.identity) as GameObject;
            Destroy(s, 1.0f);
        }
    }

    public void Dead()
    {
        if (sound)
        {
            AudioSource.PlayClipAtPoint(dead, Vector3.zero, vol);
            //print("[SoundMan] Bomba Dead!");
        }
    }

    public void Coin()
    {
        if (sound)
        {
            AudioSource.PlayClipAtPoint(coin, Vector3.zero, vol);
        }
    }
}
