﻿using UnityEngine;
using System.Collections;

public class TinyBombDestroyer : MonoBehaviour
{

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "TinyBomb")
        {
            if (other.transform.parent)
            {
                Destroy(other.transform.parent.gameObject);
            }
            else
            {
                Destroy(other.gameObject);
            }
        }
    }
}
