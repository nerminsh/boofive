﻿using UnityEngine;
using System.Collections;

public class Skin : MonoBehaviour {
    public float numberOfTinyBombs;
	public float coolDown;
	public float maxPowerOfTinyBombs;

    public Skin(float _numberOfTinyBombs, float _coolDown, float _maxPowerOfTinyBombs)
	{
        numberOfTinyBombs = _numberOfTinyBombs;
		coolDown = _coolDown;
        maxPowerOfTinyBombs = _maxPowerOfTinyBombs; 
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
