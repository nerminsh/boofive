﻿using UnityEngine;
using System.Collections;

public class Pattern : MonoBehaviour {
    public static float Speed = 1;
    public float resetDistance;
    public float initialDistance;
    public bool Active;
    void FixedUpdate()
    {
        if (gameObject.activeInHierarchy)
        {

            float move = Speed * Time.deltaTime;
            transform.Translate(Vector3.left * move, Space.World);
            if (transform.position.x < resetDistance)
            {
                transform.position = new Vector3(initialDistance, transform.position.y, transform.position.z);
                Active = false;

            }
        }
    }
}
