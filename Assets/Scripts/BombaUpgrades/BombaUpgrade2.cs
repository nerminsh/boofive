﻿using UnityEngine;
using System.Collections;

public class BombaUpgrade2 : BombaUpgradesState {

    const float _MaxNumberOfTinyBombs=12;
    const float _TinyBombPower = 3;
    const float _CoolDown = 3;
   BombaUpgradesStateMachine BUSM;

    public BombaUpgrade2(BombaUpgradesStateMachine _BUSM)
    {
        BUSM = _BUSM;
        
    }


    public float MaxNumberOfTinyBombs
    {
        get
        {
            return _MaxNumberOfTinyBombs;
        }
       
    }

    public float TinyBombPower
    {
        get
        {
            return _TinyBombPower;
        }
       
    }

   
    public float CoolDown
    {
        get
        {
            return _CoolDown;
        }
       
    }

    public void shoot(GameObject bomba, GameObject tinyBomb)
    {
        Shoot.shoot(bomba, tinyBomb);
    }
    public void dive(GameObject gameObject, float force)
    {
        Diving.dive(gameObject, force);
    }
}
