﻿using UnityEngine;
using System.Collections;

public class BombaUpgrade1 :BombaUpgradesState {
     const float _MaxNumberOfTinyBombs = 8;
     const float _TinyBombPower = 2;
     const float _CoolDown = 4;
     BombaUpgradesStateMachine BUSM;

    public BombaUpgrade1(BombaUpgradesStateMachine _BUSM)
    {
        BUSM = _BUSM;
    }
    public float MaxNumberOfTinyBombs
    {
        get
        {
            return _MaxNumberOfTinyBombs;
        }

    }

    public float TinyBombPower
    {
        get
        {
            return _TinyBombPower;
        }
       
    }

    
       

    public float CoolDown
    {
        get
        {
            return _CoolDown;
        }
       
    }

    public void shoot(GameObject bomba, GameObject tinyBomb)
    {
        Shoot.shoot(bomba, tinyBomb);
    }
   public void dive(GameObject gameObject,float force)
    {
        Diving.dive(gameObject,force);
    }

}
