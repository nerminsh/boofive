﻿using UnityEngine;
using System.Collections;

public class BombaDefault : BombaUpgradesState
{
     const float _MaxNumberOfTinyBombs = 5;
     const float _TinyBombPower = 1;
     const float _CoolDown = 5;
     BombaUpgradesStateMachine BUSM;

     public BombaDefault(BombaUpgradesStateMachine _BUSM)
    {
        BUSM = _BUSM;
    }
    public float MaxNumberOfTinyBombs
    {
        get
        {
            return _MaxNumberOfTinyBombs;
        }

    }

    public float TinyBombPower
    {
        get
        {
            return _TinyBombPower;
        }

    }
    public float CoolDown
    {
        get
        {
            return _CoolDown;
        }

    }


    public void shoot(GameObject bomba, GameObject tinyBomb)
    {
        Shoot.shoot(bomba, tinyBomb);
    }
    public void dive(GameObject gameObject, float force)
    {
        Diving.dive(gameObject, force);
    }
}
