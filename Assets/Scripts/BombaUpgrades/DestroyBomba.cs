﻿using UnityEngine;
using System.Collections;

public class DestroyBomba : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Obstacle" || other.gameObject.tag == "Wing")
        {
            Game.BombaDied = true;
        } else if (other.gameObject.tag == "Leave")
        {
            Bomba b = Game.bom;
       //    b.HitCounter = (int)b.MaxNumberOfTinyBombs1;
           b.decreeasHeating();
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Obstacle" || other.gameObject.tag == "Wing")
        {
            
            Game.BombaDied = true;

            //Destroy(gameObject);
            //Application.LoadLevel(0);
        }
    }
}
