﻿using UnityEngine;
using System.Collections;

public class SocialMediaMethods {

    private const string FACEBOOK_APP_ID = "904654399602168";
    private const string FACEBOOK_URL = "http://www.facebook.com/dialog/feed";
    private const string TWITTER_ADDRESS = "http://twitter.com/intent/tweet";
    private const string TWEET_LANGUAGE = "en";
    public static void ShareToFacebook()
    {
        string link = "https://play.google.com/store/apps/details?id=com.ITI.BombaBoo";
        string pictureLink = "http://i.imgur.com/B9ekSBc.png?1";
        string name = "I'm Playing BombaBoo";
        string caption = "a new High score";
        string description = "Just scored: " + Game.METER;
        string redirectUri = "http://m.facebook.com/";
        string url = FACEBOOK_URL + "?app_id=" + FACEBOOK_APP_ID +
         "&link=" + link +
         "&display=popup" +
         "&name=" + name +
         "&caption=" + caption +
         "&description=" + description +
         "&picture=" + pictureLink +
         "&redirect_uri=" + redirectUri;
        Debug.Log(url);
        Application.OpenURL(url);
    }

    public static void ShareToTwitter(string textToDisplay)
    {
        Application.OpenURL(TWITTER_ADDRESS +
                    "?text=" + WWW.EscapeURL(textToDisplay + PlayerPrefs.GetInt("HighScore") + "M :) - Let #Bomba live, Play #BombaBoo on Google Play NOW. https://goo.gl/aEwW2F") +
                    "&amp;lang=" + WWW.EscapeURL(TWEET_LANGUAGE));
    }	

}
