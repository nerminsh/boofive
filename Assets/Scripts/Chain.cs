﻿using UnityEngine;
using System.Collections;

public class Chain : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnDestroy()
    {
        GameObject[] Chains = transform.parent.gameObject.GetComponent<Cage>().ChainSegments;
        for (int i = 0; i < Chains.Length; i++)
        {
            if (Chains[i]) { 
                Chains[i].gameObject.GetComponent<Rigidbody2D>().fixedAngle = false;
                Destroy(Chains[i].GetComponent<CircleCollider2D>());
            }
        }
    }
}
