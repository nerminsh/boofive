﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.IO;

public class MissionManager : MonoBehaviour {
    public Mission[] Missions;
    public int MissionsCount;
    public Mission[] ActiveMissions;
    public int ActiveMissionsCount;
    public int Order;
    public static MissionManager instance;

    
    void Awake()
    {
        instance = this;
        if (!PlayerPrefs.HasKey("isFirstTime") || PlayerPrefs.GetInt("isFirstTime") == 0)
        {
            PlayerPrefs.SetInt("Order", 0);
            PlayerPrefs.SetInt("isFirstTime", 1);
            PlayerPrefs.Save();
            //try 
            //{ 
            //    PlayerPrefs.SetInt("isFirstTime", 2);
            //    PlayerPrefs.Save();
            
            //    PlayerPrefs.SetString("1Done", "false");
            //    PlayerPrefs.SetString("1InOneRun", "true");
            //    PlayerPrefs.SetString("1Active", "false");
            //    PlayerPrefs.SetInt("1Goal", 500);
            //    PlayerPrefs.SetInt("1Current", 0);
            //    PlayerPrefs.SetInt("1Order", 0);
            //    PlayerPrefs.SetInt("1Type", (int)MissionType.Distance);


            //    PlayerPrefs.SetString("2Done", "false");
            //    PlayerPrefs.SetString("2InOneRun", "true");
            //    PlayerPrefs.SetString("2Active", "false");
            //    PlayerPrefs.SetInt("2Goal", 500);
            //    PlayerPrefs.SetInt("2Current", 0);
            //    PlayerPrefs.SetInt("2Order", 0);
            //    PlayerPrefs.SetInt("2Type", (int)MissionType.CollectCoins);

            //    PlayerPrefs.SetString("3Done", "false");
            //    PlayerPrefs.SetString("3InOneRun", "true");
            //    PlayerPrefs.SetString("3Active", "false");
            //    PlayerPrefs.SetInt("3Goal", 5);
            //    PlayerPrefs.SetInt("3Current", 0);
            //    PlayerPrefs.SetInt("3Order", 0);
            //    PlayerPrefs.SetInt("3Type", (int)MissionType.CollectGems);

            //    PlayerPrefs.SetString("4Done", "false");
            //    PlayerPrefs.SetString("4InOneRun", "true");
            //    PlayerPrefs.SetString("4Active", "false");
            //    PlayerPrefs.SetInt("4Goal", 10);
            //    PlayerPrefs.SetInt("4Current", 0);
            //    PlayerPrefs.SetInt("4Order", 0);
            //    PlayerPrefs.SetInt("4Type", (int)MissionType.ShootObstacles);

            //    PlayerPrefs.SetString("5Done", "false");
            //    PlayerPrefs.SetString("5InOneRun", "false");
            //    PlayerPrefs.SetString("5Active", "false");
            //    PlayerPrefs.SetInt("5Goal", 1000);
            //    PlayerPrefs.SetInt("5Current", 0);
            //    PlayerPrefs.SetInt("5Order", 0);
            //    PlayerPrefs.SetInt("5Type", (int)MissionType.Distance);


            //    PlayerPrefs.SetString("6Done", "false");
            //    PlayerPrefs.SetString("6InOneRun", "false");
            //    PlayerPrefs.SetString("6Active", "false");
            //    PlayerPrefs.SetInt("6Current", 0);
            //    PlayerPrefs.SetInt("6Order", 0);
            //    PlayerPrefs.SetInt("6Goal", 1000);
            //    PlayerPrefs.SetInt("6Type", (int)MissionType.CollectCoins);

            //    PlayerPrefs.SetString("7Done", "false");
            //    PlayerPrefs.SetString("7InOneRun", "false");
            //    PlayerPrefs.SetString("7Active", "false");
            //    PlayerPrefs.SetInt("7Goal", 50);
            //    PlayerPrefs.SetInt("7Current", 0);
            //    PlayerPrefs.SetInt("7Order", 0);
            //    PlayerPrefs.SetInt("7Type", (int)MissionType.CollectGems);

            //    PlayerPrefs.SetString("8Done", "false");
            //    PlayerPrefs.SetString("8InOneRun", "false");
            //    PlayerPrefs.SetString("8Active", "false");
            //    PlayerPrefs.SetInt("8Goal", 500);
            //    PlayerPrefs.SetInt("8Current", 0);
            //    PlayerPrefs.SetInt("8Order", 0);
            //    PlayerPrefs.SetInt("8Type", (int)MissionType.ShootObstacles);
            //}
            //catch (System.Exception err)
            //{
            //    Debug.Log("Got: " + err);
            //}
            //PlayerPrefs.Save();
        }
        //Time.timeScale = 0;
        LoadMissions();
        for (int i = 0; i < ActiveMissionsCount; i++)
        {
            Debug.Log(ActiveMissions[i].Discription);
        }
    }
    void Start()
    {
        
    }
	// Update is called once per frame
    void LoadMissions()
    {
        using (StreamReader streamReader = new StreamReader(@"Asset\missions.json"))
        {
            string missions = streamReader.ReadToEnd();
            //JsonData jData = JsonMapper.ToObject(missions);

            JsonMission[] Ms = JsonMapper.ToObject<JsonMission[]>(missions);
            for (int i = 0; i < Ms.Length; i++)
            {
                
            }
            
        }
        Order = PlayerPrefs.GetInt("Order");
        
        
       
        ActiveMissions = new Mission[ActiveMissionsCount];

        int j = 0;

        for (int i = 0; i < MissionsCount; i++)
        {
            Missions[i] = LoadMission(i);

            if (Missions[i].Active)
            {
                ActiveMissions[j++] = Missions[i];
            }

        }
 
        /*if # already active missions is less than 3 or whatever*/
        while (j < ActiveMissionsCount)
        {
            ActiveMissions[j++] = GetMission();
        }

        /*Discription of active missions only*/
        for (int i = 0; i < ActiveMissionsCount; i++)
        {
            switch (ActiveMissions[i].Type)
            {
                case MissionType.CollectCoins:
                    ActiveMissions[i].Discription = "Collect " + 
                        ActiveMissions[i].Goal * ActiveMissions[i].Order + " coins" +
                        (ActiveMissions[i].InOneRun ? " In One Run" : "");
                    break;
                
                case MissionType.Distance:
                    ActiveMissions[i].Discription = "Travel " + 
                        ActiveMissions[i].Goal * ActiveMissions[i].Order + " meters" +
                        (ActiveMissions[i].InOneRun ? " In One Run" : "");
                    break;
                
                case MissionType.ShootObstacles:
                    ActiveMissions[i].Discription = "Shoot " +
                        ActiveMissions[i].Goal * ActiveMissions[i].Order + " obstacles" +
                        (ActiveMissions[i].InOneRun ? " In One Run" : "");
                    break;
                
                case MissionType.CollectGems:
                    ActiveMissions[i].Discription = "Collect " +
                        ActiveMissions[i].Goal * ActiveMissions[i].Order + " gems" +
                        (ActiveMissions[i].InOneRun ? " In One Run" : "");
                    break;
                
                default:
                    break;
            }
        }

    }
    Mission LoadMission(int i)
    {
        Mission m = new Mission();

        /*Order*/
        m.Order = PlayerPrefs.GetInt((i+1) + "Order");

        /*In One Run*/
        string InOneRun = PlayerPrefs.GetString((i + 1) + "InOneRun");
        if (InOneRun == "true")
        {
            m.InOneRun = true;
        }
        else
        {
            m.InOneRun = false;
        }

        /*Done*/
        string Done = PlayerPrefs.GetString((i + 1) + "Done");
        if (Done == "true")
        {
            m.Done = true;
        }
        else
        {
            m.Done = false;
        }

        /*Active*/
        string Active = PlayerPrefs.GetString((i + 1) + "Active");
        if (Active == "true")
        {
            m.InOneRun = true;
        }
        else
        {
            m.InOneRun = false;
        }

        /*Type*/
        m.Type = (MissionType)PlayerPrefs.GetInt((i + 1) + "Type");

        /*Current*/
        if (m.InOneRun || !m.Active)
        {
            m.Current = 0;
        }
        else
        {
            m.Current = PlayerPrefs.GetInt((i + 1) + "Current");
        }

        /*Goal*/
        m.Goal = PlayerPrefs.GetInt((i + 1) + "Goal");
        
        return m;
    }
    Mission GetMission()
    {
        Mission m = null;
        int k;
        if (FinishedMissions())
        {
            Order++;
            PlayerPrefs.SetInt("Order", Order);
        }
        while(m == null)
        {
            k = Random.Range(0, MissionsCount);
            if (!Missions[k].Active && Missions[k].Order < Order)
            { 
                PlayerPrefs.SetInt((k + 1) + "Order", Order);
                Missions[k].Order = Order;

                PlayerPrefs.SetString((k + 1) + "Done", "false");
                Missions[k].Done = false;

                PlayerPrefs.SetString((k + 1) + "Active", "true");
                Missions[k].Active = false;
                m = Missions[k];

            }

        }
        return m;


    }
    bool FinishedMissions()
    {
        bool finished = true;
        
        for (int i = 0; i < MissionsCount; i++)
        {
            finished &= (Missions[i].Active | Missions[i].Done);
        }
        return finished;
    }
    void Update () {
        for (int i = 0; i < ActiveMissionsCount; i++)
		{
            switch (ActiveMissions[i].Type)
            {
                case MissionType.CollectCoins:
                    break;
                case MissionType.Distance:
                    break;
                case MissionType.ShootObstacles:
                    break;
                case MissionType.CollectGems:
                    break;
                default:
                    break;
            }
            if (ActiveMissions[i].Current == ActiveMissions[i].Goal * ActiveMissions[i].Order)
            {
                ActiveMissions[i].Done = true;
                PlayerPrefs.SetString((i + 1) + "Done", "true");
            }
		}
        
	}
}
