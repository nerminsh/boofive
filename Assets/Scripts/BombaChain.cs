﻿using UnityEngine;
using System.Collections;

public class BombaChain : MonoBehaviour {
	
    private GameObject bomba;
	private Rigidbody2D bombaRigidBody;
    public GameObject obsEmitter;
    public GameObject Background;
    public GameObject parallax;
    UIManager uiMan;
    Game game;
    bool chainOk = true;
    Camera mainCamera;
    
    Vector2 startPosition;
	Vector2 endPosition;

	// Use this for initialization
	void Start () {
        uiMan = GameObject.Find("UIManager").GetComponent<UIManager>();
        game = GameObject.Find("Game").GetComponent<Game>();
		bomba = Game.bomba;
		bombaRigidBody = bomba.GetComponent<Rigidbody2D> ();
        mainCamera = Camera.main;

	}

    bool clicked = false;
    Vector3 startMouse;
    Vector3 endMouse;

	// Update is called once per frame
	void Update () {

        
        

        if(chainOk)
        {
# if UNITY_EDITOR
            if (Input.GetMouseButtonDown (0)) {
				clicked = true;
				startMouse = new Vector3 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x,
                Camera.main.ScreenToWorldPoint (Input.mousePosition).y, 0f);
			}
			if (Input.GetMouseButtonUp (0)) { 
				clicked = false;
				endMouse = new Vector3 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x,
                Camera.main.ScreenToWorldPoint (Input.mousePosition).y, 0f);
            
				RaycastHit2D[] hit = Physics2D.RaycastAll (startMouse, (endMouse - startMouse).normalized, (endMouse - startMouse).magnitude);
				Debug.DrawRay (startMouse, (endMouse - startMouse).normalized * (endMouse - startMouse).magnitude);

				//Test for cool works fine except for upward swipe ???
                if (bombaRigidBody.velocity.y < 1.5f)
                {
					bombaRigidBody.velocity = new Vector2(((endMouse - startMouse).normalized * (endMouse - startMouse).magnitude).x * 3f,
                                                                                            Mathf.Abs((int)((endMouse - startMouse).normalized * (endMouse - startMouse).magnitude).y) >= 5f ? 5f : ((endMouse - startMouse).normalized * (endMouse - startMouse).magnitude).y);
                    //Velocity UP
                    
                }

				foreach (RaycastHit2D h in hit) {
					if (h.collider) {
						if (h.collider.CompareTag ("Chain")) {
							bombaRigidBody.velocity = new Vector2 (0f,bombaRigidBody.velocity.y);
							h.collider.gameObject.transform.GetChild (0).GetComponent<HingeJoint2D> ().enabled = false;
                            transform.parent.gameObject.AddComponent<MoveBigObs>();
                            transform.parent.gameObject.GetComponent<MoveBigObs>().speed = -100f;
                            chainOk = false;
							StartGame ();
						}
					}
				}
			}
#endif
			if (Input.touchCount == 1) {
				Touch touch = Input.GetTouch (0);
				if (touch.phase == TouchPhase.Began) {
					startPosition = new Vector3 (Camera.main.ScreenToWorldPoint (touch.position).x,
				                            Camera.main.ScreenToWorldPoint (touch.position).y, 0f);
					//Debug.Log ("Began "+startPosition);
				}
				if (touch.phase == TouchPhase.Moved) {
				
					endPosition = new Vector3 (Camera.main.ScreenToWorldPoint (touch.position).x,
				                          Camera.main.ScreenToWorldPoint (touch.position).y, 0f);
					//Debug.Log ("Move "+endPosition);

					RaycastHit2D[] hit = Physics2D.RaycastAll (startPosition, (endPosition - startPosition).normalized, (endPosition - startPosition).magnitude);
					//Debug.DrawRay (startPosition, (endPosition - startPosition).normalized * (endPosition - startPosition).magnitude);

                    //Test for cool works fine except for upward swipe ???
					if (bombaRigidBody.velocity.y < 1.5f)
                    {
						bombaRigidBody.velocity = new Vector2(((endPosition - startPosition).normalized * (endPosition - startPosition).magnitude).x * 3f,
                                                                                                Mathf.Abs((int)((endPosition - startPosition).normalized * (endPosition - startPosition).magnitude).y) >= 5f ? 5f : ((endPosition - startPosition).normalized * (endPosition - startPosition).magnitude).y);
                        //Velocity UP
                        //print("[BombaChain] VelocityVector X: " + ((endPosition - startPosition).normalized * (endPosition - startPosition).magnitude).x * 3f + ", Y: " + (Mathf.Abs((int)((endPosition - startPosition).normalized * (endPosition - startPosition).magnitude).y) >= 5f ? 5f : ((endPosition - startPosition).normalized * (endPosition - startPosition).magnitude).y));
                    }

                    foreach (RaycastHit2D h in hit) {
						if (h.collider) {
							if (h.collider.CompareTag ("Chain")) {
								bombaRigidBody.velocity = new Vector2 (0f, bombaRigidBody.velocity.y);
								h.collider.gameObject.transform.GetChild (0).GetComponent<HingeJoint2D> ().enabled = false;
								//Destroy(h.collider.gameObject);
                                transform.parent.gameObject.AddComponent<MoveBigObs>();
                                transform.parent.gameObject.GetComponent<MoveBigObs>().speed = -100f;
                                chainOk = false;
								StartGame ();
							}
						}
					}
				}
			}
		}
	}

	void StartGame()
    {
      //  print("[BombaChain] Game Started!");
        Destroy(bomba.GetComponent<MoveX>());
        bomba.GetComponent<Bomba>().enabled = true;
        //bomba.transform.GetChild(0).gameObject.SetActive(true);
        Camera.main.GetComponent<FollowY>().enabled = true;
        uiMan.PlayFromMain();
        StartCoroutine("MoveCamera");
        parallax.SetActive(true);
        Background.SetActive(false);

        //MOVE THAT TO UPDATE 

        //Camera.main.GetComponent<Animator>().SetTrigger("ChainCut");
        //game.enabled = true;
        //obsEmitter.SetActive(true);

        //Hide then Destroy Me after you finish! or disable
        //transform.parent.gameObject.AddComponent<MoveBigObs>();
        //transform.parent.gameObject.GetComponent<MoveBigObs>().speed = -100f;
        //Destroy(gameObject, 2.0f);
    }
    IEnumerator MoveCamera()
    {
        while (true)
        {

            mainCamera.orthographicSize = Mathf.SmoothStep(mainCamera.orthographicSize, 7, 5*Time.deltaTime);
            //mainCamera.transform.position = Vector3.MoveTowards(mainCamera.transform.position,new Vector3(0, mainCamera.transform.position.y, mainCamera.transform.position.z),4*Time.deltaTime);

            AspectUtility.SetCamera();
			if(mainCamera.orthographicSize >= 6.9)
				mainCamera.rect = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
            //Camera.main.transform.position = Vector2.MoveTowards(bomba.transform.position, new Vector2(0, transform.position.y),1/Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }
}
