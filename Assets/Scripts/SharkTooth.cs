﻿using UnityEngine;
using System.Collections;

public class SharkTooth : MonoBehaviour {
    public float speed;
	public int HeatIncreaeAmmount = 2;
	Rigidbody2D rBody;
	void Start(){
		rBody = GetComponent<Rigidbody2D> ();
	}
    void Update()
    {
		rBody.velocity = (new Vector3(-speed, 0));

    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Bomba")
        {
            
			Game.bom.HitCounter += HeatIncreaeAmmount;
			Bomba.heatingRatio = Game.bom.HitCounter / (int)Game.bom.MaxNumberOfTinyBombs1;
            Destroy(gameObject);
        }
    }
}
