﻿using UnityEngine;
using System.Collections;

public interface PowerUpState  {
     void PowerBehavior(Bomba b);
     int GetTime();
     IEnumerator SetBehavior( Bomba b);
}
