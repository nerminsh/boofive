﻿using UnityEngine;
using System.Collections;

public interface TinyBombState  {

	 float Speed
    {
        get;
    }
    float Power
    {
        get; 
    }
    void Move(GameObject tinyBomb); 
	}

