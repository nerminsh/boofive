﻿using UnityEngine;
using System.Collections;

public interface BombaUpgradesState {
    float MaxNumberOfTinyBombs
    {
        get;
    }
    float TinyBombPower
    {
        get; 
    }
   
    float CoolDown
    {
        get;
    }
     void shoot(GameObject bomba, GameObject tinyBomb);
     void dive(GameObject gameObject, float force);
         //   public void dive();

   
}

