﻿using UnityEngine;
using System.Collections;

public class PowerUpsStateMachine : MonoBehaviour
{
    PowerUpState noPowerUP;
    GameObject bomba;
    PowerUpState unlimitedTinyBombs;
    PowerUpState powerUpState;
    public PowerUpsStateMachine()
    {
        bomba = Game.bomba;
        noPowerUP = bomba.AddComponent(typeof(NoPowerUP)) as NoPowerUP;
           // new NoPowerUP(this);
           // unlimitedTinyBombs = new UnlimitedTinyBombs(this);
        powerUpState = noPowerUP;
          //Debug.Log("state pusm" + noPowerUP);

    }
    public override string ToString()
    {
        return "PowerUpStateMachine Object";
    }
    public void setPowerUP(PowerUpState newPowerUpState)
    {
        powerUpState = newPowerUpState;
    }
    public void changePowerUp(PowerUpState newPowerUpState)
    {
        powerUpState = newPowerUpState;

    }
    public PowerUpState NoPowerUP
    {
        get { return noPowerUP; }
    }

    public PowerUpState UnlimitedTinyBombs
    {
        get { return unlimitedTinyBombs; }
    }
    public PowerUpState getCurrentPowerUpState()
    {
        return powerUpState;
    }
    public void defaultState(){

        powerUpState = noPowerUP;
    }
    public void SetBehavior(Bomba b)
    {
        powerUpState.PowerBehavior(b);
    }

}
