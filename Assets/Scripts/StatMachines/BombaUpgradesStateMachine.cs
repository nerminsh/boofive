﻿using UnityEngine;
using System.Collections;

public class BombaUpgradesStateMachine : MonoBehaviour {
    BombaUpgradesState bombaDefault;
    BombaUpgradesState bombaUpgrade1;


    BombaUpgradesState bombaUpgrade2;

   
    BombaUpgradesState bombaUpgradeState;

    public BombaUpgradesStateMachine()
    {
         bombaDefault = new BombaDefault(this); 
       
        bombaUpgradeState=bombaDefault;
    }
    public void upgradeBomba(BombaUpgradesState newBombaUpgradeState)
    {
        bombaUpgradeState = newBombaUpgradeState;
        
    }
    public void setBombaUpgradeState(BombaUpgradesState _bombaUpgradeState)
    {
        bombaUpgradeState = _bombaUpgradeState;
    }
    public BombaUpgradesState BombaDefault
    {
        get { return bombaDefault; }
    }

    public BombaUpgradesState BombaUpgrade1
    {
        get { return bombaUpgrade1; }
    }
    public BombaUpgradesState BombaUpgrade2
    {
        get { return bombaUpgrade2; }
    }
    public BombaUpgradesState getCurrentUpgradeState()
    {
        return bombaUpgradeState;
    }
	// Use this for initialization
	
   
  
}
