﻿using UnityEngine;
using System.Collections;

public class TinyBombStateMachine  {
    TinyBombState tinyBomb;
    TinyBombState tinyBomb1;
    TinyBombState tinyBomb2;
    TinyBombState tinyBombState;
    public TinyBombStateMachine()
    {
         tinyBomb = new TinyBomb(this);

         tinyBombState = tinyBomb;
    }
    public void upgradeBomba(TinyBombState newTinyBomb)
    {
        tinyBombState = newTinyBomb;
    }
    public void setTinyeState(TinyBombState _TinyBombState)
    {
        tinyBombState = _TinyBombState;
    }
    public TinyBombState TinyBomb
    {
        get { return tinyBomb; }
    }
    public TinyBombState TinyBomb1
    {
        get { return tinyBomb1; }
    }
    public TinyBombState TinyBomb2
    {
        get { return tinyBomb2; }
    }
    public TinyBombState getCurrentTinyBombState()
    {
        return tinyBombState; ;
    }
}
