﻿using UnityEngine;
using System.Collections;

public class Shark : MonoBehaviour {

    Seeker seeker;
    int target = 1;
    public GameObject bullit;
    public GameObject Teeth;
    public GameObject Bomba;
    public float ReloadTime;
    public float BulletRateOfFire;
    public float TeethRateOfFire;
    public float NextBulletTime = 0.0f;
    public float NextTeethTime = 0.0f;
    public int BulletsCount;
    public int BulletsNum;
    public int TeethCount;
    public int TeethNum;
    public bool TeethFirst;
    public bool start;
    public static int health = 20;

    void Start()
    {
        NextBulletTime = int.MaxValue;
        NextTeethTime = int.MaxValue;

        seeker = GetComponent<Seeker>();
        if (seeker)
        {
            seeker.target = new Vector2(8, Bomba.transform.position.y);
        }
    }

    // Update is called once per frame
    void Update()
    {


        if (transform.position.x <= 8 && start)
        {
            TeethFirst = Random.Range(0, 11) >= 5;
            if (TeethFirst) {
                NextTeethTime = Time.time + 2;
                NextBulletTime = Time.time + ReloadTime + TeethCount * TeethRateOfFire;
            }
            else
            {
                NextBulletTime = Time.time + 2;
                NextTeethTime = Time.time + ReloadTime + BulletsCount * BulletRateOfFire;
            }
            start = false;
        }

        else if (start)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(7, 0), 0.3f);
        }

        if (Time.time >= NextBulletTime && Time.time < NextTeethTime)
        {

            Instantiate(bullit, new Vector2(transform.position.x - GetComponent<Renderer>().bounds.size.x / 2, transform.position.y), Quaternion.identity);
            BulletsNum++;

            if (BulletsNum == BulletsCount)
            {
                BulletsNum = 0;
                NextBulletTime += (ReloadTime + TeethCount * TeethRateOfFire);
            }
            else
            {
                NextBulletTime += BulletRateOfFire;
            }

        }
        if (Time.time >= NextTeethTime && Time.time < NextBulletTime)
        {

            Instantiate(Teeth, new Vector2(transform.position.x - GetComponent<Renderer>().bounds.size.x / 2, transform.position.y), Quaternion.identity);
            TeethNum++;

            if (TeethNum == TeethCount)
            {
                TeethNum = 0;
                NextTeethTime += (ReloadTime + BulletsCount * BulletRateOfFire);
            }
            else
            {
                NextTeethTime += TeethRateOfFire;
            }

        }
        
        if (Bomba)
        {
            seeker.target = new Vector2(8, Bomba.transform.position.y);
        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "TinyBomb" && --health == 0)
        {
            Destroy(gameObject);
        }
        //else if (other.tag == "Bomba")
        //{
        //    Destroy(other.gameObject);
        //    Application.LoadLevel(0);
        //}

    }
}
