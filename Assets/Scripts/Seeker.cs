﻿using UnityEngine;
using System.Collections;

public class Seeker : MonoBehaviour {

	public float MAX_SPEED;
    public float Speed;
	Rigidbody2D  rBody;
    public Vector2 target;


	// Use this for initialization
	void Start () {
		rBody = GetComponent<Rigidbody2D> ();
	
	}
	
	// Update is called once per frame
	void Update () {
        if (target != null)
        {
            if (Vector2.Distance(transform.position, target) > 0.1f)
            {
				rBody.velocity = Truncate(Seek(target) + rBody.velocity, Speed);
            }
            else
            {
				rBody.velocity = Vector2.zero;
            }
        }
	}
    private Vector2 Seek(Vector2 target)
    {
        Vector2 desiredVel = target - (Vector2)gameObject.transform.position;
        desiredVel = desiredVel.normalized * MAX_SPEED;
		Vector2 steeringForce = desiredVel - rBody.velocity;
        return steeringForce;
    }
    private Vector2 Truncate(Vector2 vector, float speed)
    {
        if (vector.magnitude > speed)
            return vector.normalized * speed;
        else
            return vector;
    }
}
