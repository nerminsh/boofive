﻿using UnityEngine;
using System.Collections;

public class RotatePattern : MonoBehaviour {

    public float rotateSpeed = 1f;
	Rigidbody2D  rBody;
	// Use this for initialization
	void Start () {
		rBody = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
        //transform.Rotate(new Vector3(0f,0f,transform.rotation.z+1),Space.World);
        //Debug.Log("transform.localRotation.z " + transform.localRotation.z);
        
        // Added by Arsh :D Thx to Nemo >;(
		rBody.angularVelocity = rotateSpeed;
	}
}
