﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Babaya : MonoBehaviour {

    public bool start;
    public float subLvlUp = 500;
    public float ReloadTime;
    public float RateOfFire;
    public float NextBulletTime = 0.0f;
    public float Health = 7;
    public int BulletsCount;
    public int num = 4;
    public int Ammo = 3;
    public int MAX_AMMO = 14;
    public GameObject bullit;
    public GameObject Bomba;
    public GameObject ex;
    public GameObject HealthBar;
    Seeker seeker;
    Image hb;
    Renderer r;
    float h = 0;
    int NoOfBullits = 0;
    int target = 1;
    void Start()
    {
        hb = HealthBar.GetComponent<Image>();
        hb.fillAmount = 1;
        r = GetComponent<Renderer>();
        SetAmmo();
        NextBulletTime = int.MaxValue;
        seeker = GetComponent<Seeker>();
        if (seeker)
        {
            seeker.target = new Vector2(8, Bomba.transform.position.y);
        }
    }

    IEnumerator Confused()
    {
        NextBulletTime = int.MaxValue;
        ex.SetActive(true);
        yield return new WaitForSeconds(5);
        ex.SetActive(false);
        transform.rotation = Quaternion.Euler(0, 180, 0);
        Destroy(gameObject.GetComponent<Seeker>());
        GetComponent<Submarine>().finishedAmmo = true;
        GetComponent<Rigidbody2D>().AddForce(new Vector2(0.2f, 0), ForceMode2D.Impulse);
        Destroy(gameObject, 2);
    }
    // Update is called once per frame
    void Update()
    {
        if (transform.position.x <= 8 && start)
        {
            NextBulletTime = Time.time + 2;
            start = false;
        }
        else if (start)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(7, 0), 0.3f);
        }

        if (Time.time >= NextBulletTime)
        {
            
            for (int i = -1; i < 2; i++)
            {
                bool instantiate = Random.Range(0, 10) < 4;

                if (transform.position.y - (i) * 2.5f >= -14 && instantiate)
                    Game.Obstacles.Add((GameObject)Instantiate(bullit, new Vector2(transform.position.x - 
                        r.bounds.size.x / 2, transform.position.y - (i) * 2.5f), Quaternion.identity));
            }

            NoOfBullits++;
            if (NoOfBullits == BulletsCount)
            {
                NoOfBullits = 0;
                Ammo--;
                NextBulletTime += ReloadTime;
            }
            else
            {
                NextBulletTime += RateOfFire;
            }

        }
        if (h <= 0 && Bomba)
        {
            StartCoroutine("Confused");
        }
        else if (Bomba)
        {
            seeker.target = new Vector2(8, Bomba.transform.position.y);
        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Bomba")
        {
            //Destroy(other.gameObject);
            Game.BombaDied = true;
            //Application.LoadLevel(0);
        }
        else if (other.tag == "TinyBomb")
        {
            h--;
            hb.fillAmount = h / Health;

        }

    }

    // Added by Arshd :D
    private void SetAmmo()
    {
        Health = (Health + (int)(Game.METER / subLvlUp)) > MAX_AMMO ? MAX_AMMO : Health + (int)(Game.METER / subLvlUp);
        h = Health;
    }

    private void SetBulletSpeed()
    {
        bullit.GetComponent<Bullet>().speed = ((int)(Game.METER / subLvlUp)) + bullit.GetComponent<Bullet>().speed;
    }
    // EOA = End of Arsh :D
}
