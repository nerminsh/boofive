﻿using UnityEngine;
using System.Collections;

public class Submarine : MonoBehaviour {

    public bool finishedAmmo = false;
    public GameObject particleEffect;
    void Awake()
    {
        Ewais ewais = GetComponent<Ewais>();
        Babaya babaya = GetComponent<Babaya>();
        Geno geno = GetComponent<Geno>();

        if (ewais)
        {
            ewais.Bomba = GameObject.FindGameObjectWithTag("Bomba");
        }
        if (babaya)
        {
            babaya.Bomba = GameObject.FindGameObjectWithTag("Bomba");
        }
        if (geno)
        {
            geno.Bomba = GameObject.FindGameObjectWithTag("Bomba");
        }
    }
    void OnTriggerEnter2D(Collider2D Other)
    {
        if(Other.tag == "TinyBomb")
        {
            if (particleEffect)
            {
                GameObject g = (GameObject)Instantiate(particleEffect, new Vector2(Other.transform.position.x + Other.GetComponent<Renderer>().bounds.size.x / 2, Other.transform.position.y), Quaternion.identity);
                g.transform.parent = transform;
                
            }
            Destroy(Other.gameObject);
        }

    }
}
