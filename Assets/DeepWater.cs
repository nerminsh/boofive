﻿using UnityEngine;
using System.Collections;

public class DeepWater : MonoBehaviour {

    Transform Deep;
    public GameObject[] obstacles;

    // Use this for initialization
    void Start()
    {
        Deep = GameObject.FindGameObjectWithTag("Deep").transform;
    }
    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bomba")
        {
            instantiatObstacle(obstacles);
        }
    }
    void instantiatObstacle(GameObject[] obses)
    {
        StartCoroutine("InstObstacl", obses);
        //while (!obj)
        //{
        //    obj = (GameObject)Instantiate(obses[obs], Above.transform.position, Quaternion.identity);
        //}
        // obj.transform.parent = this.transform.parent;
    }
    IEnumerator InstObstacl(GameObject[] obses)
    {
        yield return new WaitForSeconds(1);
        System.Random r = new System.Random();
        int obs = r.Next(obses.Length);
        GameObject obj = (GameObject)Instantiate(obses[obs], Deep.position, Quaternion.identity);
    }
}
