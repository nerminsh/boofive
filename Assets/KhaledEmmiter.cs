﻿using UnityEngine;
using System.Collections;

public class KhaledEmmiter : MonoBehaviour {

    public int timeMax = 10;
    public int timeMin = 3;
    int slicesCounter = 1;
    int slicesCount = 3;
    int PatternsCount = 0;
    int SubmarineType = 0;
    public GameObject[] GameObjects;
    public GameObject[] Coins;
    public GameObject[] Submarines;
    public GameObject submarine;
    float time;
    float CoinsTime;
    float SubmarinesTime;
    int i = 0;

    // Use this for initialization
    void Start()
    {
        time = Time.time;
        CoinsTime = int.MaxValue;
        SubmarinesTime = int.MaxValue;

    }

    // Update is called once per frame
    //void Update () {

    //}
    void FixedUpdate()
    {
        if (Time.time >= time)
        {

            Instantiate(GameObjects[Random.Range(0, GameObjects.Length)],
                new Vector2(gameObject.transform.position.x, 0),
                Quaternion.identity);
            slicesCounter++;

            if (slicesCounter == slicesCount)
            {
                slicesCounter = 0;
                PatternsCount++;
                if (PatternsCount == 3)
                {
                    PatternsCount = 0;
                    CoinsTime = int.MaxValue;
                    SubmarinesTime = Time.time + timeMax / 3;
                    time = int.MaxValue;

                }
                else
                {
                    time = Time.time + timeMax;
                    CoinsTime = Time.time + (timeMax / 2);
                }
                if (Game.METER < 2000)
                {
                    slicesCount = Easy();
                }
                else if (Game.METER < 4000)
                {
                    slicesCount = Medium();
                }
                else
                {
                    slicesCount = Hard();
                }

                time = Time.time + timeMax;
            }
            else
            {
                time = Time.time + timeMin;
            }

        }
        if (Time.time >= SubmarinesTime && Time.time < time)
        {
            SubmarinesTime = int.MaxValue;
            submarine = (GameObject)Instantiate(Submarines[i++],
               new Vector2(gameObject.transform.position.x, 0),
               Quaternion.identity);
            if (i == Submarines.Length)
            {
                i = 0;
            }
        }
        if (Time.time >= CoinsTime && Time.time < time)
        {
          //  bool emmitCoins = Random.Range(0, 10) <= 7;
            bool emmitCoins = false;
            if (emmitCoins)
            {
                Instantiate(Coins[Random.Range(0, GameObjects.Length)],
                    new Vector2(gameObject.transform.position.x,
                        Random.Range(-2, 2)),
                        Quaternion.identity);
            }
            CoinsTime = int.MaxValue;
        }
        if (submarine && submarine.GetComponent<Submarine>().finishedAmmo)
        {
            time = Time.time + timeMax;
        }

    }
    int Easy()
    {
        int randNum = Random.Range(1, 10);
        slicesCounter = 0;
        if (randNum <= 7)
        {
            return 3;
        }
        else if (randNum <= 9)
        {
            return 4;
        }
        else
        {
            return 5;
        }
    }
    int Medium()
    {
        int randNum = Random.Range(1, 10);
        slicesCounter = 0;
        if (randNum <= 7)
        {
            return 4;
        }
        else if (randNum <= 9)
        {
            return 3;
        }
        else
        {
            return 5;
        }
    }
    int Hard()
    {
        int randNum = Random.Range(1, 10);
        slicesCounter = 0;
        if (randNum <= 7)
        {
            return 5;
        }
        else if (randNum <= 9)
        {
            return 4;
        }
        else
        {
            return 3;
        }

    }
}
