﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Move : MonoBehaviour {
    public float speed = 0.2f;
	Rigidbody2D rBody;
	// Use this for initialization
	void Start () {
		if ((0.2f + Game.METER/10000) <= 0.5f) {
			speed = speed + Game.METER/10000;;
		} 
		else {
			speed = 0.5f;
		}
		rBody = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
			

		rBody.velocity = new Vector2(-speed/Time.deltaTime, rBody.velocity.y);
	}
}
