﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(Floating))]
public class FloatingEditor : Editor
{
    public override void OnInspectorGUI()
    {
        Floating ft = (Floating)target;
        ft.CalculateDensity();
        float Density = EditorGUILayout.FloatField("Density : ",ft.density);
        ft.ResetMass(Density);
    }
    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {
	
    //}
}
